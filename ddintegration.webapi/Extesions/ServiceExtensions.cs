﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using Docusign.Model.Persistence;
using Docusign.Repositories.Repositories.Interfaces;
using Docusign.Repositories.Repositories;
using ddsign.webapi.Services.Interfaces;
using ddsign.webapi.Services;
using System.Net.Http;
using ddintegration.webapi.Client.Interfaces;
using ddintegration.webapi.Client;

namespace ddintegration.webapi.Extesions
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder => builder.AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials()
                );
            });
        }

        public static void ConfigureIISIntegration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {

            });
        }

        public static void ConfigureSqlServerContext(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<DocusignDbContext>(options => options.UseSqlServer(config.GetConnectionString("Development")), ServiceLifetime.Singleton);
        }

        public static void ConfigureRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IUserDocusignedRepository, UserDocusignRepository>();
            services.AddSingleton<ISignedDocusignRepository, SignedDocusignRepository>();
            services.AddSingleton<ILogRepositories, LogRepositories>();
        }

        public static void ConfigureServices(this IServiceCollection services)
        {
            services.AddScoped<IDocusingService, DocusignService>();
           
        }
        public static void ConfigureClient(this IServiceCollection services)
        {
            services.AddScoped<IDigitaldocClient, DigitaldocClient>();
        }

        //public static IApplicationBuilder UseHttpStatusCodeExceptionMiddleware(this IApplicationBuilder builder)
        //{
        //    return builder.UseMiddleware<HttpStatusCodeExceptionMiddleware>();
        //}

    }
}
