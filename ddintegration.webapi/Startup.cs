﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ddintegration.webapi.Extesions;
using ddsign.webapi.Services;
using ddsign.webapi.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ddintegration.webapi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.ConfigureSqlServerContext(Configuration);

            services.AddHttpClient();

            services.ConfigureCors();

            services.ConfigureIISIntegration();           

            services.ConfigureRepositories();

            services.ConfigureServices();
            services.AddMvc(options =>
            {
                options.ReturnHttpNotAcceptable = true;
                options.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
#pragma warning disable CS0618 // O tipo ou membro é obsoleto
                options.InputFormatters.Add(new XmlSerializerInputFormatter());
#pragma warning restore CS0618 // O tipo ou membro é obsoleto
                options.OutputFormatters.Add(new XmlSerializerOutputFormatter());

                options.FormatterMappings.SetMediaTypeMappingForFormat("xml", "application/xml");
            });
            services.AddHttpsRedirection(options =>
            {                
                options.HttpsPort = 443;
            });
            services.AddMvc();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
