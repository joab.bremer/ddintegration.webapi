﻿
using Docusign.Model.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ddsign.webapi.Services.Interfaces
{
    public interface IDocusingService
    {
        TokenModel Login(HttpRequest request);
        bool Docusing(XElement XMLElement, TokenModel userAuthenticate);
    }
}
