﻿using ddintegration.webapi.Client;
using ddsign.webapi.Controllers;
using ddsign.webapi.Extesions;
using ddsign.webapi.Services.Interfaces;
using Digitaldoc.Model;
using Digitaldoc.Model.Enums;
using Docusign.Model.Models;
using Docusign.Model.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ddsign.webapi.Services
{
    public class DocusignService : IDocusingService
    {
        public TokenModel Login(HttpRequest request)
        {

            string loginEmail = null;
            string password = null;


            var headers = request.Headers;
            string authorization = headers["Authorization"];
            if (authorization != null && authorization.StartsWith("Basic"))
            {
                string encodedUsernamePassword = authorization.Substring("Basic ".Length).Trim();
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
                int seperatorIndex = usernamePassword.IndexOf(':');

                loginEmail = usernamePassword.Substring(0, seperatorIndex);
                password = usernamePassword.Substring(seperatorIndex + 1);
            }
            else
            {
                //Handle what happens if that isn't the case
                throw new Exception("The authorization header is either empty or isn't Basic.");
            }


            
            TokenModel token = null;
            IRestResponse result = null;

            //login via usuário e senha pela api do digitaldoc 
            if (loginEmail != null && password != null)
            {
                DigitaldocClient digitaldocClient = new DigitaldocClient();
                result = digitaldocClient.Login(loginEmail, password);

                token = JsonConvert.DeserializeObject<TokenModel>(result.Content); ;
            }
            
           
            return token;
        }


        public bool Docusing(XElement XMLElement, TokenModel tokenModel)
        {
            try
            {
                Revision revision;     
                List<Revision> revisionList = new List<Revision>();

                string token = tokenModel.access_token;
                //Iniciando extração dos elementos do XML
                var digitaldocClient = new DigitaldocClient();
                string xmlNoNameSpace = DocusignExtension.RemoveAllNamespaces(XMLElement.ToString());

                //Convertendo XML para XMLDocument
                XmlDocument xD = new XmlDocument();
                xD.LoadXml(xmlNoNameSpace);                

                //Extraindo Elemento de 'DocumentPDF'
                XmlNodeList xnList = xD.GetElementsByTagName("DocumentPDF");
                foreach (XmlNode xn in xnList)
                {
                    revision = new Revision();
                    revision.Title = xn["Name"].InnerText;


                    revision.FileData = Convert.FromBase64String(xn["PDFBytes"].InnerText);
                                       
                    revision.Integration = EIntegration.DOCUSIGN;
                    revision.Extension = "pdf";
                    //chamar post para salvar revision
                    ConnectionLostHandler.IsOnline = true;                    
                    revisionList.Add(revision);


                }
                
                var run = Task.Run(() => digitaldocClient.PostRevisionAsync(revisionList, token));
                run.Wait();
                if (!run.Result.IsSuccessStatusCode)
                {
                     return false;
                }
                return true;
            }
            catch(Exception ex)
            {

                return false ;
            }
           
                        
        }
    }
}
