﻿using System;
using System.Net.Http;
using System.Xml.Linq;
using ddsign.webapi.Services;
using Docusign.Model.Models;
using Docusign.Repositories.Repositories;
using Docusign.Repositories.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace ddsign.webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocusignController : ControllerBase
    {
        private readonly ILogRepositories _logRepository;
        private Logger _logger;

        public DocusignController(ILogRepositories LogRepositories)
        {
            _logRepository = LogRepositories;
             _logger = LogManager.GetCurrentClassLogger();

        }
        
        [HttpPost]
        public ActionResult Post([FromBody]XElement xml)
        {
            bool result = false;           
            var response = new HttpResponseMessage();
            var log = new Log()
            {
                IdUser = null,
                Date = DateTime.Now,
                Url = "api/docusign"
            };
            

            DocusignService docusignService = new DocusignService();
            //Relaizando login
            TokenModel token = docusignService.Login(Request);
            if (token != null)
            {
                log.Message = "token gerado com com sucesso!";
                result = docusignService.Docusing(xml, token);
            }else
            {
                log.Message = "Falha ao gerar token";
            }

            if (!result)
            {
                log.Message = "Falha ao salvar documento.";
                // _logRepository.Save(log);
                return BadRequest();
            }

            log.Message = "Documento salvo com sucesso!";
           // _logRepository.Save(log);
            return Ok();

        }
    
    }
}