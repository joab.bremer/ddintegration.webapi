﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ddintegration.webapi.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    public class ErroController : ControllerBase
    {
        [AllowAnonymous]
        public ActionResult Get()
        {

            return BadRequest("Consulta invalida!");
        }
    }
}
