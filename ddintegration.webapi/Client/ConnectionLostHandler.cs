﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ddintegration.webapi.Client
{
    public class ConnectionLostHandler : DelegatingHandler
    {
        private volatile static bool _isOnline = true;

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            try
            {
                if (IsOnline)
                {
                    var response = await base.SendAsync(request, cancellationToken);
                    return response;
                }
                else
                {
                    return new HttpResponseMessage(HttpStatusCode.ServiceUnavailable);
                }

            }
            catch (HttpRequestException ex)
            {
                if (IsOnline && !request.RequestUri.PathAndQuery.EndsWith("/logoff"))
                {
                    IsOnline = false;
                    throw ex;
                }
                return new HttpResponseMessage(HttpStatusCode.ServiceUnavailable);
            }

        }

        public static bool IsOnline
        {
            get { return _isOnline; }
            set
            {
                _isOnline = value;
            }
        }
    }
}
