﻿using ddintegration.webapi.Client.Interfaces;
using ddintegration.webapi.Extesions;
using Digitaldoc.Model;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ddintegration.webapi.Client
{
    public class DigitaldocClient : IDigitaldocClient
    {
        public IRestResponse Login(string loginEmail, string password)
        {
            string passmd5 = DigitaldocExtension.CreateMD5(password);
        
            var client = new RestClient("https://dev-digitaldoc.azurewebsites.net/token");
            //var client = new RestClient("https://localhost:44300/token");
            var request = new RestRequest(Method.POST);

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            request.AddParameter("undefined", "grant_type=password&" +
                "username=" + loginEmail + "&" +
                "password=" + passmd5 + "&" +
                "client_id=0b0ec116af2ab6e87a16db93ce7bb88cbf219e74bf7e11250b6e9ee1fc756ff2&" +
                "client_secret=f9c7a373013a47b16400e73f146cb867ebcfb000bc457744ed2298387b0e735f", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response;
        }

        public async Task<HttpResponseMessage> PostRevisionAsync(List<Revision> revisionList, string token)
        {            

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "https://dev-digitaldoc.azurewebsites.net/integrationv2/docusign");
            //HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "https://localhost:44300/integrationv2/docusign");
            string json = JsonConvert.SerializeObject(revisionList);
            request.Content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            HttpClient http = new HttpClient();
            http.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpResponseMessage response = await http.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {   
                
                var saved = await response.Content.ReadAsAsync<Revision>();       
                
                if(revisionList != null)
                {
                    return response;
                }                
            }

            return response;


        }

        public async Task<Revision> PostRevision(Revision revision, string token)
        {
            if (!ConnectionLostHandler.IsOnline)
            {
                return null;
            }

            using (var client = HttpClientFactory.Create()){

                HttpResponseMessage response;
                

                client.BaseAddress = new Uri("https://dev-digitaldoc.azurewebsites.net");

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);                

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                response = await client.PostAsJsonAsync("revisions/docusign", revision);

                if (response.IsSuccessStatusCode)
                {

                    var saved = await response.Content.ReadAsAsync<Revision>();

                    if (saved != null)
                    {
                        return saved;
                    }
                }

            }
            return null;
            

        }
    }
}
