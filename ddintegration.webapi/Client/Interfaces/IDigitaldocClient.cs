﻿using Digitaldoc.Model;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ddintegration.webapi.Client.Interfaces
{
    public interface IDigitaldocClient
    {
        IRestResponse Login(string loginEmail, string password);
        Task<HttpResponseMessage> PostRevisionAsync(List<Revision> revisions, string token);
        Task<Revision> PostRevision(Revision revision, string token);
    }
}
