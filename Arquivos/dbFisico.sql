CREATE DATABASE Docusign;
GO

USE [Docusign]
GO

CREATE TABLE logs(
	Id bigint NOT NULL IDENTITY    PRIMARY KEY,
	Message nvarchar(max) NULL,
	Url nvarchar(max) NULL,
	Date datetime NULL,
	id_user bigint NULL
)


CREATE TABLE token(
	Id bigint NOT NULL IDENTITY    PRIMARY KEY,
	access_token nvarchar(max) NULL,
	expires_in nvarchar(max) NULL,
	refresh_token nvarchar(max) NULL,
	token_type nvarchar(max) NULL
)


CREATE TABLE [dbo].[user](
	Id bigint NOT NULL IDENTITY    PRIMARY KEY,
	email nvarchar(max) NULL,
	password nvarchar(max) NULL
)