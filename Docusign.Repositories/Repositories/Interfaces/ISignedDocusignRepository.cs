﻿using Docusign.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Docusign.Repositories.Repositories.Interfaces
{
    public interface ISignedDocusignRepository
    {
        void Save(EnveloperDocusign enveloper);
    }
}
