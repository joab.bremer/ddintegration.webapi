﻿using Docusign.Model.Models;
using Docusign.Model.Persistence;
using Docusign.Repositories.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Docusign.Repositories.Repositories
{
    public class UserDocusignRepository: IUserDocusignedRepository
    {
        protected DocusignDbContext db;
        public UserDocusignRepository()
        {
            db = new DocusignDbContext();
        }

        public void Save(UserDocusign userDocusign)
        {
            db.UserDocusigns.Add(userDocusign);
            db.SaveChanges();
        }
    }
}
