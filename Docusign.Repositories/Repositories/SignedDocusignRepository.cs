﻿using Docusign.Model.Models;
using Docusign.Model.Persistence;
using Docusign.Repositories.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Docusign.Repositories.Repositories
{ 

    public class SignedDocusignRepository : ISignedDocusignRepository
    {
        protected DocusignDbContext db;
        public SignedDocusignRepository()
        {
            db = new DocusignDbContext();
        }

        public void Save(EnveloperDocusign enveloper)
        {
            db.EnveloperDocusigns.Add(enveloper);
            db.SaveChanges();
        }
    }
}
