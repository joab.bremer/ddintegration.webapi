﻿using Docusign.Model.Models;
using Docusign.Model.Persistence;
using Docusign.Repositories.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Docusign.Repositories.Repositories
{
    public class LogRepositories: ILogRepositories
    {
        protected DocusignDbContext db;
        public LogRepositories()
        {
            db = new DocusignDbContext();
        }

        public void Save(Log log)
        {
            db.Log.Add(log);
            db.SaveChanges();
        }
    }
}
