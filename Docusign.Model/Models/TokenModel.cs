﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Docusign.Model.Models
{
    [Table("token")]
    public class TokenModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Column("access_token")]
        public string access_token { get; set; }
        [Column("expires_in")]
        public int expires_in { get; set; }
        [Column("refresh_token")]
        public string refresh_token { get; set; }
        [Column("token_type")]
        public string token_type { get; set; }
    }
}
