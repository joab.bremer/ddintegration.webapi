﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Docusign.Model.Models
{
    [Table("logs")]
    public class Log
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Column("id_user")]
        public long? IdUser { get; set; }
        public DateTime Date { get; set; }
        public string Url { get; set; }
        public string Message { get; set; }
    }
}
