﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Docusign.Model.Models
{
    [Table("Enveloper")]
    public class EnveloperDocusign
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Column("id_user")]
        public long UserId { get; set; }
        [Column("xml_byte")]
        public byte[] Xml { get; set; }
    }
}
