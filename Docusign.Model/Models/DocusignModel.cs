﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Docusign.Model.Models
{
    public class DocusignModel
    {
        // OBSERVAÇÃO: o código gerado pode exigir pelo menos .NET Framework 4.5 ou .NET Core/Standard 2.0.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.docusign.net/API/3.0", IsNullable = false)]
        public partial class DocuSignEnvelopeInformation
        {

            private DocuSignEnvelopeInformationEnvelopeStatus envelopeStatusField;

            private DocuSignEnvelopeInformationDocumentPDF[] documentPDFsField;

            private string timeZoneField;

            private sbyte timeZoneOffsetField;

            /// <remarks/>
            public DocuSignEnvelopeInformationEnvelopeStatus EnvelopeStatus
            {
                get
                {
                    return this.envelopeStatusField;
                }
                set
                {
                    this.envelopeStatusField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("DocumentPDF", IsNullable = false)]
            public DocuSignEnvelopeInformationDocumentPDF[] DocumentPDFs
            {
                get
                {
                    return this.documentPDFsField;
                }
                set
                {
                    this.documentPDFsField = value;
                }
            }

            /// <remarks/>
            public string TimeZone
            {
                get
                {
                    return this.timeZoneField;
                }
                set
                {
                    this.timeZoneField = value;
                }
            }

            /// <remarks/>
            public sbyte TimeZoneOffset
            {
                get
                {
                    return this.timeZoneOffsetField;
                }
                set
                {
                    this.timeZoneOffsetField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        public partial class DocuSignEnvelopeInformationEnvelopeStatus
        {

            private DocuSignEnvelopeInformationEnvelopeStatusRecipientStatuses recipientStatusesField;

            private System.DateTime timeGeneratedField;

            private string envelopeIDField;

            private string subjectField;

            private string userNameField;

            private string emailField;

            private string statusField;

            private System.DateTime createdField;

            private System.DateTime sentField;

            private System.DateTime deliveredField;

            private System.DateTime signedField;

            private System.DateTime completedField;

            private string aCStatusField;

            private System.DateTime aCStatusDateField;

            private string aCHolderField;

            private string aCHolderEmailField;

            private string aCHolderLocationField;

            private string signingLocationField;

            private string senderIPAddressField;

            private object envelopePDFHashField;

            private DocuSignEnvelopeInformationEnvelopeStatusCustomField[] customFieldsField;

            private bool autoNavigationField;

            private bool envelopeIdStampingField;

            private bool authoritativeCopyField;

            private DocuSignEnvelopeInformationEnvelopeStatusDocumentStatuses documentStatusesField;

            /// <remarks/>
            public DocuSignEnvelopeInformationEnvelopeStatusRecipientStatuses RecipientStatuses
            {
                get
                {
                    return this.recipientStatusesField;
                }
                set
                {
                    this.recipientStatusesField = value;
                }
            }

            /// <remarks/>
            public System.DateTime TimeGenerated
            {
                get
                {
                    return this.timeGeneratedField;
                }
                set
                {
                    this.timeGeneratedField = value;
                }
            }

            /// <remarks/>
            public string EnvelopeID
            {
                get
                {
                    return this.envelopeIDField;
                }
                set
                {
                    this.envelopeIDField = value;
                }
            }

            /// <remarks/>
            public string Subject
            {
                get
                {
                    return this.subjectField;
                }
                set
                {
                    this.subjectField = value;
                }
            }

            /// <remarks/>
            public string UserName
            {
                get
                {
                    return this.userNameField;
                }
                set
                {
                    this.userNameField = value;
                }
            }

            /// <remarks/>
            public string Email
            {
                get
                {
                    return this.emailField;
                }
                set
                {
                    this.emailField = value;
                }
            }

            /// <remarks/>
            public string Status
            {
                get
                {
                    return this.statusField;
                }
                set
                {
                    this.statusField = value;
                }
            }

            /// <remarks/>
            public System.DateTime Created
            {
                get
                {
                    return this.createdField;
                }
                set
                {
                    this.createdField = value;
                }
            }

            /// <remarks/>
            public System.DateTime Sent
            {
                get
                {
                    return this.sentField;
                }
                set
                {
                    this.sentField = value;
                }
            }

            /// <remarks/>
            public System.DateTime Delivered
            {
                get
                {
                    return this.deliveredField;
                }
                set
                {
                    this.deliveredField = value;
                }
            }

            /// <remarks/>
            public System.DateTime Signed
            {
                get
                {
                    return this.signedField;
                }
                set
                {
                    this.signedField = value;
                }
            }

            /// <remarks/>
            public System.DateTime Completed
            {
                get
                {
                    return this.completedField;
                }
                set
                {
                    this.completedField = value;
                }
            }

            /// <remarks/>
            public string ACStatus
            {
                get
                {
                    return this.aCStatusField;
                }
                set
                {
                    this.aCStatusField = value;
                }
            }

            /// <remarks/>
            public System.DateTime ACStatusDate
            {
                get
                {
                    return this.aCStatusDateField;
                }
                set
                {
                    this.aCStatusDateField = value;
                }
            }

            /// <remarks/>
            public string ACHolder
            {
                get
                {
                    return this.aCHolderField;
                }
                set
                {
                    this.aCHolderField = value;
                }
            }

            /// <remarks/>
            public string ACHolderEmail
            {
                get
                {
                    return this.aCHolderEmailField;
                }
                set
                {
                    this.aCHolderEmailField = value;
                }
            }

            /// <remarks/>
            public string ACHolderLocation
            {
                get
                {
                    return this.aCHolderLocationField;
                }
                set
                {
                    this.aCHolderLocationField = value;
                }
            }

            /// <remarks/>
            public string SigningLocation
            {
                get
                {
                    return this.signingLocationField;
                }
                set
                {
                    this.signingLocationField = value;
                }
            }

            /// <remarks/>
            public string SenderIPAddress
            {
                get
                {
                    return this.senderIPAddressField;
                }
                set
                {
                    this.senderIPAddressField = value;
                }
            }

            /// <remarks/>
            public object EnvelopePDFHash
            {
                get
                {
                    return this.envelopePDFHashField;
                }
                set
                {
                    this.envelopePDFHashField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("CustomField", IsNullable = false)]
            public DocuSignEnvelopeInformationEnvelopeStatusCustomField[] CustomFields
            {
                get
                {
                    return this.customFieldsField;
                }
                set
                {
                    this.customFieldsField = value;
                }
            }

            /// <remarks/>
            public bool AutoNavigation
            {
                get
                {
                    return this.autoNavigationField;
                }
                set
                {
                    this.autoNavigationField = value;
                }
            }

            /// <remarks/>
            public bool EnvelopeIdStamping
            {
                get
                {
                    return this.envelopeIdStampingField;
                }
                set
                {
                    this.envelopeIdStampingField = value;
                }
            }

            /// <remarks/>
            public bool AuthoritativeCopy
            {
                get
                {
                    return this.authoritativeCopyField;
                }
                set
                {
                    this.authoritativeCopyField = value;
                }
            }

            /// <remarks/>
            public DocuSignEnvelopeInformationEnvelopeStatusDocumentStatuses DocumentStatuses
            {
                get
                {
                    return this.documentStatusesField;
                }
                set
                {
                    this.documentStatusesField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        public partial class DocuSignEnvelopeInformationEnvelopeStatusRecipientStatuses
        {

            private DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatus recipientStatusField;

            /// <remarks/>
            public DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatus RecipientStatus
            {
                get
                {
                    return this.recipientStatusField;
                }
                set
                {
                    this.recipientStatusField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        public partial class DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatus
        {

            private string typeField;

            private string emailField;

            private string userNameField;

            private byte routingOrderField;

            private System.DateTime sentField;

            private System.DateTime deliveredField;

            private System.DateTime signedField;

            private object declineReasonField;

            private string statusField;

            private string recipientIPAddressField;

            private object[] customFieldsField;

            private DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusTabStatus[] tabStatusesField;

            private DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusRecipientAttachment recipientAttachmentField;

            private string accountStatusField;

            private DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusFormData formDataField;

            private string recipientIdField;

            /// <remarks/>
            public string Type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }

            /// <remarks/>
            public string Email
            {
                get
                {
                    return this.emailField;
                }
                set
                {
                    this.emailField = value;
                }
            }

            /// <remarks/>
            public string UserName
            {
                get
                {
                    return this.userNameField;
                }
                set
                {
                    this.userNameField = value;
                }
            }

            /// <remarks/>
            public byte RoutingOrder
            {
                get
                {
                    return this.routingOrderField;
                }
                set
                {
                    this.routingOrderField = value;
                }
            }

            /// <remarks/>
            public System.DateTime Sent
            {
                get
                {
                    return this.sentField;
                }
                set
                {
                    this.sentField = value;
                }
            }

            /// <remarks/>
            public System.DateTime Delivered
            {
                get
                {
                    return this.deliveredField;
                }
                set
                {
                    this.deliveredField = value;
                }
            }

            /// <remarks/>
            public System.DateTime Signed
            {
                get
                {
                    return this.signedField;
                }
                set
                {
                    this.signedField = value;
                }
            }

            /// <remarks/>
            public object DeclineReason
            {
                get
                {
                    return this.declineReasonField;
                }
                set
                {
                    this.declineReasonField = value;
                }
            }

            /// <remarks/>
            public string Status
            {
                get
                {
                    return this.statusField;
                }
                set
                {
                    this.statusField = value;
                }
            }

            /// <remarks/>
            public string RecipientIPAddress
            {
                get
                {
                    return this.recipientIPAddressField;
                }
                set
                {
                    this.recipientIPAddressField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("CustomField", IsNullable = false)]
            public object[] CustomFields
            {
                get
                {
                    return this.customFieldsField;
                }
                set
                {
                    this.customFieldsField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("TabStatus", IsNullable = false)]
            public DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusTabStatus[] TabStatuses
            {
                get
                {
                    return this.tabStatusesField;
                }
                set
                {
                    this.tabStatusesField = value;
                }
            }

            /// <remarks/>
            public DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusRecipientAttachment RecipientAttachment
            {
                get
                {
                    return this.recipientAttachmentField;
                }
                set
                {
                    this.recipientAttachmentField = value;
                }
            }

            /// <remarks/>
            public string AccountStatus
            {
                get
                {
                    return this.accountStatusField;
                }
                set
                {
                    this.accountStatusField = value;
                }
            }

            /// <remarks/>
            public DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusFormData FormData
            {
                get
                {
                    return this.formDataField;
                }
                set
                {
                    this.formDataField = value;
                }
            }

            /// <remarks/>
            public string RecipientId
            {
                get
                {
                    return this.recipientIdField;
                }
                set
                {
                    this.recipientIdField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        public partial class DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusTabStatus
        {

            private string tabTypeField;

            private string statusField;

            private ushort xPositionField;

            private ushort yPositionField;

            private string tabLabelField;

            private string tabNameField;

            private string tabValueField;

            private byte documentIDField;

            private byte pageNumberField;

            private string customTabTypeField;

            /// <remarks/>
            public string TabType
            {
                get
                {
                    return this.tabTypeField;
                }
                set
                {
                    this.tabTypeField = value;
                }
            }

            /// <remarks/>
            public string Status
            {
                get
                {
                    return this.statusField;
                }
                set
                {
                    this.statusField = value;
                }
            }

            /// <remarks/>
            public ushort XPosition
            {
                get
                {
                    return this.xPositionField;
                }
                set
                {
                    this.xPositionField = value;
                }
            }

            /// <remarks/>
            public ushort YPosition
            {
                get
                {
                    return this.yPositionField;
                }
                set
                {
                    this.yPositionField = value;
                }
            }

            /// <remarks/>
            public string TabLabel
            {
                get
                {
                    return this.tabLabelField;
                }
                set
                {
                    this.tabLabelField = value;
                }
            }

            /// <remarks/>
            public string TabName
            {
                get
                {
                    return this.tabNameField;
                }
                set
                {
                    this.tabNameField = value;
                }
            }

            /// <remarks/>
            public string TabValue
            {
                get
                {
                    return this.tabValueField;
                }
                set
                {
                    this.tabValueField = value;
                }
            }

            /// <remarks/>
            public byte DocumentID
            {
                get
                {
                    return this.documentIDField;
                }
                set
                {
                    this.documentIDField = value;
                }
            }

            /// <remarks/>
            public byte PageNumber
            {
                get
                {
                    return this.pageNumberField;
                }
                set
                {
                    this.pageNumberField = value;
                }
            }

            /// <remarks/>
            public string CustomTabType
            {
                get
                {
                    return this.customTabTypeField;
                }
                set
                {
                    this.customTabTypeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        public partial class DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusRecipientAttachment
        {

            private DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusRecipientAttachmentAttachment attachmentField;

            /// <remarks/>
            public DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusRecipientAttachmentAttachment Attachment
            {
                get
                {
                    return this.attachmentField;
                }
                set
                {
                    this.attachmentField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        public partial class DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusRecipientAttachmentAttachment
        {

            private string dataField;

            private string labelField;

            /// <remarks/>
            public string Data
            {
                get
                {
                    return this.dataField;
                }
                set
                {
                    this.dataField = value;
                }
            }

            /// <remarks/>
            public string Label
            {
                get
                {
                    return this.labelField;
                }
                set
                {
                    this.labelField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        public partial class DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusFormData
        {

            private DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusFormDataXfdf xfdfField;

            /// <remarks/>
            public DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusFormDataXfdf xfdf
            {
                get
                {
                    return this.xfdfField;
                }
                set
                {
                    this.xfdfField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        public partial class DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusFormDataXfdf
        {

            private DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusFormDataXfdfField[] fieldsField;

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("field", IsNullable = false)]
            public DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusFormDataXfdfField[] fields
            {
                get
                {
                    return this.fieldsField;
                }
                set
                {
                    this.fieldsField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        public partial class DocuSignEnvelopeInformationEnvelopeStatusRecipientStatusesRecipientStatusFormDataXfdfField
        {

            private string valueField;

            private string nameField;

            /// <remarks/>
            public string value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string name
            {
                get
                {
                    return this.nameField;
                }
                set
                {
                    this.nameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        public partial class DocuSignEnvelopeInformationEnvelopeStatusCustomField
        {

            private string nameField;

            private bool showField;

            private bool requiredField;

            private string valueField;

            private string customFieldTypeField;

            /// <remarks/>
            public string Name
            {
                get
                {
                    return this.nameField;
                }
                set
                {
                    this.nameField = value;
                }
            }

            /// <remarks/>
            public bool Show
            {
                get
                {
                    return this.showField;
                }
                set
                {
                    this.showField = value;
                }
            }

            /// <remarks/>
            public bool Required
            {
                get
                {
                    return this.requiredField;
                }
                set
                {
                    this.requiredField = value;
                }
            }

            /// <remarks/>
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }

            /// <remarks/>
            public string CustomFieldType
            {
                get
                {
                    return this.customFieldTypeField;
                }
                set
                {
                    this.customFieldTypeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        public partial class DocuSignEnvelopeInformationEnvelopeStatusDocumentStatuses
        {

            private DocuSignEnvelopeInformationEnvelopeStatusDocumentStatusesDocumentStatus documentStatusField;

            /// <remarks/>
            public DocuSignEnvelopeInformationEnvelopeStatusDocumentStatusesDocumentStatus DocumentStatus
            {
                get
                {
                    return this.documentStatusField;
                }
                set
                {
                    this.documentStatusField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        public partial class DocuSignEnvelopeInformationEnvelopeStatusDocumentStatusesDocumentStatus
        {

            private byte idField;

            private string nameField;

            private object templateNameField;

            private byte sequenceField;

            /// <remarks/>
            public byte ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }

            /// <remarks/>
            public string Name
            {
                get
                {
                    return this.nameField;
                }
                set
                {
                    this.nameField = value;
                }
            }

            /// <remarks/>
            public object TemplateName
            {
                get
                {
                    return this.templateNameField;
                }
                set
                {
                    this.templateNameField = value;
                }
            }

            /// <remarks/>
            public byte Sequence
            {
                get
                {
                    return this.sequenceField;
                }
                set
                {
                    this.sequenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.docusign.net/API/3.0")]
        public partial class DocuSignEnvelopeInformationDocumentPDF
        {

            private string nameField;

            private string pDFBytesField;

            private byte documentIDField;

            private bool documentIDFieldSpecified;

            private string documentTypeField;

            /// <remarks/>
            public string Name
            {
                get
                {
                    return this.nameField;
                }
                set
                {
                    this.nameField = value;
                }
            }

            /// <remarks/>
            public string PDFBytes
            {
                get
                {
                    return this.pDFBytesField;
                }
                set
                {
                    this.pDFBytesField = value;
                }
            }

            /// <remarks/>
            public byte DocumentID
            {
                get
                {
                    return this.documentIDField;
                }
                set
                {
                    this.documentIDField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlIgnoreAttribute()]
            public bool DocumentIDSpecified
            {
                get
                {
                    return this.documentIDFieldSpecified;
                }
                set
                {
                    this.documentIDFieldSpecified = value;
                }
            }

            /// <remarks/>
            public string DocumentType
            {
                get
                {
                    return this.documentTypeField;
                }
                set
                {
                    this.documentTypeField = value;
                }
            }
        }
    }
}