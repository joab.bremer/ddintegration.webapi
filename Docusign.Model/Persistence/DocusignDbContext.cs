﻿using Microsoft.EntityFrameworkCore;
using Docusign.Model.Models;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Docusign.Model.Persistence
{
    public class DocusignDbContext : DbContext
    {

        public DocusignDbContext()
        {
        }

        public DocusignDbContext(DbContextOptions<DocusignDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)            
        {
            /*
            if (!optionsBuilder.IsConfigured)
            {
                var configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build();

                var connectionString = configuration.GetConnectionString("Docusign");
                optionsBuilder.UseSqlServer(connectionString);
            }          */  
        }

        public DbSet<EnveloperDocusign> EnveloperDocusigns { get; set; }
        public DbSet<Log> Log { get; set; }
        public DbSet<TokenModel> TokenModel { get; set; }
        public DbSet<UserDocusign> UserDocusigns { get; set; }
        

    }
}