﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Digitaldoc.Model.Resources;

namespace Digitaldoc.Model.Integration
{
    public class RevisionIntegration
    {

        public RevisionIntegration()
        {

        }

        public RevisionIntegration(Revision revision)
        {
            Id = revision.Id;
            IdFolder = revision.IdFolder;
            IdDocumentType = revision.IdDocumentType;
            Code = revision.Code;
            Title = revision.Title;
            Description = revision.Description;
            RevisionNumber = revision.RevisionNumber;
            Date = revision.Date;
            KeyWords = revision.Keywords;
            Extension = revision.Extension;
            Size = revision.Size;
            IdStatus = revision.IdStatus;
            AdditionalFields = new List<RevisionAdditionalFieldsIntegration>();
            if (revision.AdditionalFields != null)
            {
                foreach (var af in revision.AdditionalFields)
                {
                    AdditionalFields.Add(new RevisionAdditionalFieldsIntegration(af));
                }
            }
        }

        public long Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Digitaldoc.Model.Resources.Resources), ErrorMessageResourceName = "fieldRequired")]
        public long? IdFolder { get; set; }

        [Required(ErrorMessageResourceType = typeof(Digitaldoc.Model.Resources.Resources), ErrorMessageResourceName = "fieldRequired")]
        public long? IdDocumentType { get; set; }
        
        public string Code { get; set; }

        [Required(ErrorMessageResourceType = typeof(Digitaldoc.Model.Resources.Resources), ErrorMessageResourceName = "fieldRequired")]
        public string Title { get; set; }

        public string Description { get; set; }

        public string KeyWords { get; set; }
        
        public int RevisionNumber { get; set; }

        public DateTime Date { get; set; }

        [Required(ErrorMessageResourceType = typeof(Digitaldoc.Model.Resources.Resources), ErrorMessageResourceName = "fieldRequired")]
        public string Extension { get; set; }

        [Required(ErrorMessageResourceType = typeof(Digitaldoc.Model.Resources.Resources), ErrorMessageResourceName = "fieldRequired")]
        public long Size { get; set; }

        public List<RevisionAdditionalFieldsIntegration> AdditionalFields { get; set; }

        public int IdStatus { get; set; }
    }
}