﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Integration
{
    public class DocumentTypeIntegration
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<AdditionalFieldIntegration> AdditionalFields { get; set; }
    }
}