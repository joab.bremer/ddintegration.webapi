﻿using System.ComponentModel.DataAnnotations;
namespace Digitaldoc.Model.Integration
{
    public class RevisionAdditionalFieldsIntegration
    {

        public RevisionAdditionalFieldsIntegration(RevisionAdditionalField af)
        {
            Id = af.IdAdditionalField;
            Value = af.Value;
            Name = af.AdditionalField.Name;
        }

        public RevisionAdditionalFieldsIntegration()
        {

        }

        /// <summary>
        /// Id do campo adicional no banco de dados. Pode ser null para conseguir fazer a validação do model
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Digitaldoc.Model.Resources.Resources), ErrorMessageResourceName = "fieldRequired")]
        public long? Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Digitaldoc.Model.Resources.Resources), ErrorMessageResourceName = "fieldRequired")]
        public string Value { get; set; }

        public string Name { get; set; }

    }
}