﻿using System.ComponentModel.DataAnnotations;

namespace Digitaldoc.Model.Integration
{
    public class AdditionalFieldIntegration
    {

        public long Id { get; set; }        
        
        public int IdFieldType { get; set; }

        public int ValidationType { get; set; }

        public string Name { get; set; }

        public string List { get; set; }

        public bool IsRequired { get; set; }
    }
}