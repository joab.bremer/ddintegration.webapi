﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Integration
{
    public class FolderIntegration
    {
        public long Id { get; set; }
        public long? IdParent { get; set; }
        public String Name { get; set; }
        public bool CanSendDocs { get; set; }
        public bool CanDeleteDocs { get; set; }
        public bool HasChildren { get; set; }
    }
}