﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.DTO
{
    public class RevisionResource
    {

        public int IdStatus { get; set; }
        public long IdFolder { get; set; }
        public long IdDocumentType { get; set; }
        public string Code { get; set; }
            public string Date { get; set; }
            public string Title { get; set; }
        public List<int> Features { get; set; }
        

    }
}