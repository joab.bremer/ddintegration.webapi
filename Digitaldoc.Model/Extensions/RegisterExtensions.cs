﻿using System.Linq;

namespace Digitaldoc.Model.Extensions
{
    public static class RegisterExtensions
    {
        public static void TrimAllStrings(this Registro entity)
        {
            var stringProperties = entity.GetType().GetProperties().Where(p => p.PropertyType == typeof(string) && p.CanWrite);

            foreach (var stringProperty in stringProperties)
            {
                string currentValue = (string)stringProperty.GetValue(entity, null);

                if (!string.IsNullOrEmpty(currentValue))
                {
                    stringProperty.SetValue(entity, currentValue.Trim(), null);
                }
            }
        }
    }
}