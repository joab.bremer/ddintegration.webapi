﻿using System;
using System.Linq;
using System.Reflection;

namespace Digitaldoc.Model.Extensions
{
    public static class ReflectionExtension
    {

        /// <summary>
        /// Gets a property's parent object
        /// </summary>
        /// <param name="sourceObject">Source object</param>
        /// <param name="propertyPath">Path of the property (ex: Prop1.Prop2.Prop3 would be
        /// the Prop1 of the source object, which then has a Prop2 on it, which in turn
        /// has a Prop3 on it.)</param>
        /// <returns>The ancestor property's info</returns>
        public static object GetPropertyInfo(this object sourceObject, string propertyPath, out PropertyInfo propertyInfo)
        {
            if (sourceObject == null)
            {
                propertyInfo = null;
                return null;
            }

            char[] splitter = {'.'};
            var sourceProperties = propertyPath.Split(splitter, StringSplitOptions.None);

            object tempSourceProperty = sourceObject;

            var propertyType = sourceObject.GetType();
            propertyInfo = propertyType.GetProperty(sourceProperties[0]);

            for (int i = 1; i < sourceProperties.Length; ++i)
            {

                if (tempSourceProperty != null)
                {
                    tempSourceProperty = propertyInfo.GetValue(tempSourceProperty, null);
                }

                propertyType = propertyInfo.PropertyType;
                propertyInfo = propertyType.GetProperty(sourceProperties[i]);
            }

            return tempSourceProperty;
        }


        /// <summary>
        /// Gets a property's parent object
        /// </summary>
        /// <param name="sourceObject">Source object</param>
        /// <param name="methodPath">Path of the property (ex: Prop1.Prop2.Prop3 would be
        /// the Prop1 of the source object, which then has a Prop2 on it, which in turn
        /// has a Prop3 on it.)</param>
        /// <returns>The ancestor property's info</returns>
        public static object GetMethodInfo(this object sourceObject, string methodPath, out MethodInfo methodInfo)
        {
            if (sourceObject == null)
            {
                methodInfo = null;
                return null;
            }

            char[] splitter = { '.' };
            var sourceProperties = methodPath.Split(splitter, StringSplitOptions.None);

             object tempSourceProperty = sourceObject;

            var propertyType = sourceObject.GetType();
            var propertyInfo = propertyType.GetProperty(sourceProperties[0]);

            if (sourceProperties.Length > 2)
            {
                for (int i = 1; i < (sourceProperties.Length - 1); ++i)
                {
                    if (tempSourceProperty != null)
                    {
                        tempSourceProperty = propertyInfo.GetValue(tempSourceProperty, null);
                    }
                    propertyType = propertyInfo.PropertyType;
                    propertyInfo = propertyType.GetProperty(sourceProperties[i]);
                }
            }

            if (propertyInfo == null)
            {
                methodInfo = null;
                return null;
            }

            tempSourceProperty = propertyInfo.GetValue(tempSourceProperty, null);
            propertyType = propertyInfo.PropertyType;
            methodInfo = propertyType.GetMethod(sourceProperties.Last());

            return tempSourceProperty;
        }

        
    }
}