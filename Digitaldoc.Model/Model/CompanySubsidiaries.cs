﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Model
{
    [Table("company_subsidiaries")]
    public class CompanySubsidiaries : Registro
    {
        [Column("name")]
        public string Name { get; set; }

        [Column("cnpj")]
        public string Cnpj { get; set; }

        [Column("uf")]
        public string Uf { get; set; }

        [Column("id_folder")]
        public long IdFolder { get; set; }

        [Column("id_company")]
        public long IdCompany { get; set; }

        [ForeignKey("IdFolder")]
        public virtual Folder Folder { get; set; }

        [ForeignKey("IdCompany")]
        public virtual Company Company { get; set; }


    }
}