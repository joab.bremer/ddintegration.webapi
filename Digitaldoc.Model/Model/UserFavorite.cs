﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Digitaldoc.Model
{
    [Table("users_favorites")]
    public class UserFavorite
    {
        [Key]
        [Column("id_revision", Order = 1)]
        public long IdRevision { get; set; }

        [Key]
        [Column("id_user", Order = 2)]
        public long IdUser { get; set; }

        [ForeignKey("IdRevision")]
        public Revision Revision { get; set; }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj.GetType() != GetType())
            {
                return false;
            }

            var other = (UserFavorite)obj;
            return IdRevision == other.IdRevision && IdUser == other.IdUser;
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return (IdUser + IdRevision).GetHashCode();
        }

    }
}