﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Model.AutoComplete
{
    public class AutoComplete
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

    }
}