﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Model
{
	public class NfeDocument
	{
		public string AccessKey { get; set; }

		public short Mod
		{
			get { return Convert.ToInt16(AccessKey.Substring(20, 2)); }
		}

		public short Serial
		{
			get { return Convert.ToInt16(AccessKey.Substring(22, 3)); }
		}

		public long Number
		{
			get { return Convert.ToInt64(AccessKey.Substring(25, 9)); }
		}

		public decimal Amount { get; set; }

		public string IssuerName { get; set; }

		public string IssuerCnpj { get; set; }
		public string DhEmi { get; set; }
		public string DhRecbto { get; set; }
		public decimal versao { get; set; }
		public byte cSitNFe { get; set; }
		public ulong nProt { get; set; }
		public string digVal { get; set; }
		public string ProxyDhEmi { get; set; }
		public string IE { get; set; }
		public string CPF { get; set; }

		public byte tpNF { get; set; }

		public byte[] Nfe { get; set; }


		//INFORMAÇÕES VINDAS DE OUTRA CLASSE

		public ulong novoCNPJ { get; set; }
		public string tpEvento { get; set; }
	}
}