﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("groups_folders_roles")]
    public class GroupFolderRole : IEquatable<GroupFolderRole>
    {
        [Key]
        [Column("id_group", Order=0)]
        public long IdGroup { get; set; }
        [Key]
        [Column("id_folder",Order = 1)]
        public long IdFolder { get; set; }
        [Key]
        [Column("id_role",Order = 2)]
        public long IdRole { get; set; }

        public override int GetHashCode()
        {
            return IdGroup.GetHashCode() ^ IdFolder.GetHashCode() ^ IdRole.GetHashCode();
        }

        public bool Equals(GroupFolderRole obj)
        {
            if (obj != null && obj is GroupFolderRole) {

                var gfr = (GroupFolderRole)obj;

                if(gfr.IdFolder == IdFolder && gfr.IdGroup == IdGroup && gfr.IdRole == IdRole)
                {
                    return true;
                }

            }

            return false;
        }
    }
}