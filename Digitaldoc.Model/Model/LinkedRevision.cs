﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("linked_revisions")]
    public class LinkedRevision
    {

        [Key]
        [Column("id_main_revision", Order = 0)]
        public long IdMainRevision { get; set; }

        [Key]
        [Column("id_linked_revision", Order = 1)]
        public long IdLinkedRevision { get; set; }

        [ForeignKey("IdMainRevision")]
        public virtual Revision MainRevision { get; set; }

        [ForeignKey("IdLinkedRevision")]
        public virtual Revision RevisionLinked { get; set; }

    }
}