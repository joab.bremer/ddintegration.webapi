﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
     [Table("document_types_folders")]
    public class DocumentTypesFolders
    {
        [Key]
        [Column("id_document_type", Order = 0)]
        public long IdDocumentType { get; set; }
        [Key]
        [Column("id_folder", Order = 1)]
        public long IdFolder { get; set; }

        [ForeignKey("IdDocumentType")]
        public virtual DocumentType DocumentType { get; set; }
    }
}