﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("revisions_sequences")]
    public class RevisionSequences : Registro
    {
        [Column("document_type_acronym")]
        public string DocumentTypeAcronym { get; set; }
        [Column("folder_acronym")]
        public string FolderAcronym { get; set; }
        public long Sequence { get; set; }
        [Column("id_company")]
        public long IdCompany { get; set; }
    }
}