﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public class SharedRevisionRoles
    {
        
        public DateTime SharedDate { get; set; }
        
        public long IdFolder { get; set; }
        
        public string SharedBy { get; set; }
        
        public List<Revision> Revisions { get; set; }
        
        public User Sender { get; set; }
        
        public User Receiver { get; set; }
        
        public List<Role> Roles { get; set; }

    }
}