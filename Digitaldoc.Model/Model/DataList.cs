﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("lists")]
    public class DataList : Registro
    {
        [Column("id_company")]
        public long IdCompany { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Informe itens para a lista")]
        public string Items { get; set; }

        public int Removed { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is DataList)
            {
                var list = (DataList)obj;

                return list.Id == Id && list.IdCompany == IdCompany && list.Name == Name;
            }

            return false;
        }
    }
}