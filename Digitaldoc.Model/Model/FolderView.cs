﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("folders_view")]
    public class FolderView 
    {
        [Key]
        public long Id{ get; set; }
        [Column("id_parent")]
        public long? IdParent { get; set; }
        [Column("id_company")]
        public long IdCompany { get; set; }

        [Column("inherit_acronym")]
        public Boolean InheritAcronym { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Acronym { get; set; }
        public string Path { get; set; }

        public int Removed { get; set; }
    }
}