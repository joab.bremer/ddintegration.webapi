﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("document_types_additional_fields")]
    public class DocumentTypeAdditionalFields
    {
        [Key]
        [Column("id_document_type", Order = 0)]
        public long IdDocumentType { get; set; }
        [Key]
        [Column("id_additional_field", Order = 1)]
        public long IdAdditionalField { get; set; }

        [Column("is_required")]
        public bool Required { get; set; }

        [ForeignKey("IdAdditionalField")]
        public virtual AdditionalField AdditionalField { get; set; }
        
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var af = (DocumentTypeAdditionalFields)obj;
            if(this.IdAdditionalField == af.IdAdditionalField && this.IdDocumentType == af.IdDocumentType)
            {
                return true;
            }

            return base.Equals(obj);
        }
        
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}