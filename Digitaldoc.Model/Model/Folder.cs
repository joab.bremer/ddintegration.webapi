﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("folders")]
    public class Folder : Registro
    {
        [Column("id_parent")]
        public long? IdParent { get; set; }
        [Column("id_company")]
        public long IdCompany { get; set; }

        [Required]
        [Column("inherit_acronym")]
        public Boolean InheritAcronym { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public string Description { get; set; }
        [MaxLength(100)]
        public string Acronym { get; set; }
        [Column("path")]
        public string Path { get; set; }

        public int Removed { get; set; }

        [Column("id_migrated_folder")]
        public long? IdMigrateFolder { get; set; }

        [NotMapped]
        public Folder Parent { get; set; }

        public virtual List<DocumentTypesFolders> DocumentTypes { get; set; }
    }
}