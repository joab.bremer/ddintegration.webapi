﻿using Digitaldoc.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("document_types")]
    public class DocumentType : Registro
    {
        [Column("id_company")]
        public long IdCompany { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "fieldRequired")]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "fieldRequired")]
        public string Acronym { get; set; }
        [Column("revision_interval")]
        public int? RevisionInterval { get; set; }
        [Column("notification_days")]
        public int? NotificationDays { get; set; }
        [Column("can_override_rev_interval")]
        public bool CanOverrideRevInterval { get; set; }

        public int Removed { get; set; }

        public virtual List<DocumentTypeAdditionalFields> DocumentTypeAdditionalFields { get; set; }
    }
}