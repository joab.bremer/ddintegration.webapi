﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Digitaldoc.Model
{
    [Table("exported_medias")]
    public class ExportedMedia : Registro
    {
        [Column("id_user")]
        public long IdUser { get; set; }
        [Column("id_company")]
        public long IdCompany { get; set; }
        [Column("id_folder")]
        public long IdFolder { get; set; }
     
        public string Name { get; set; }

        [Column("date_created")]
        public DateTime DateCreated { get; set; }
        [Column("date_started")]
        public DateTime? DateStarted { get; set; }
        [Column("date_finished")]
        public DateTime? DateFinished { get; set; }

        public EMediaStatus Status { get; set; }
        [Column("media_type")]
        public EMediaType MediaType { get; set; }

        public string Error { get; set; }

        public int Removed { get; set; }

        [ForeignKey("IdFolder")]
        public virtual Folder Folder {get;set;}
        [ForeignKey("IdCompany")]
        public virtual Company Company { get; set; }
        
    }
}