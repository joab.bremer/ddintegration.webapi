﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("revisions_checkouts")]
    public class RevisionCheckout : Registro
    {
        [Column("id_revision")]
        public long IdRevision { get; set; }
        [Column("id_user")]
        public long IdUser { get; set; }
        public DateTime Date { get; set; }
        [Column("resolved_date")]
        public DateTime? ResolvedDate { get; set; }
        [Column("id_old_revision")]
        public long? IdOldRevision { get; set; }
        [Column("resolved")]
        public int Resolved { get; set; }


        [ForeignKey("IdUser")]
        public virtual User User { get; set; }
        
    }
}