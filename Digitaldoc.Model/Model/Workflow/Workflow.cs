﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Digitaldoc.Model.WF
{
    [Table("wf_flows")]
    public class Workflow : Registro
    {

        [Column("name")]
        public string Name { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("start_type")]
        public ETriggerType StartType { get; set; }

        [Column("is_active")]
        public bool IsActive { get; set; }

        [Column("id_company")]
        public long IdCompany { get; set; }

        [Column("removed")]
        public bool Removed { get; set; }

        [Column("row_guid")]
        public Guid RowGuid { get; set; }

    }
}