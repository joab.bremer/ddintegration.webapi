﻿using Digitaldoc.Model.Enums.Workflow;
using Digitaldoc.Model.WF;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Digitaldoc.Model.Workflow
{
    [Table("wf_executions")]
    public class Execution : Registro
    {
        [Column("id_flow_revision")]
        public long IdFlowRevision { get; set; }
        [Column("id_revision")]
        public long IdRevision { get; set; }
  
        [Column("start_date")]
        public DateTime? StartDate { get; set; }
        [Column("end_date")]
        public DateTime? EndDate { get; set; }

        [Column("status")]
        public EExecutionStatus Status { get; set; }

        [ForeignKey("IdFlowRevision")]
        public virtual WorkflowRevision WorkflowRevision { get; set; }
        [ForeignKey("IdRevision")]
        public virtual Revision Revision { get; set; }

    }
}