﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Digitaldoc.Model.Workflow
{
    [Table("wf_execution_histories")]
    public class ExecutionHistory : Registro
    {
        [Column("id_execution_step")]
        public long IdExecutionStep { get; set; }
        [Column("id_user")]
        public long? IdUser { get; set; }
        public string Action { get; set; }
        public string Comments { get; set; }
        public string Form { get; set; }
        public DateTime? Date { get; set; }

        [ForeignKey("IdExecutionStep")]
        public virtual ExecutionStep Step { get; set; }
        [ForeignKey("IdUser")]
        public virtual User User { get; set; }
        
    }
}