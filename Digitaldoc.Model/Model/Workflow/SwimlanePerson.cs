﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    [Table("wf_lane_persons")]
    public class SwimlanePerson : Registro
    {
        
        [Column("id_lane")]
        public long IdSwimlane { get; set; }
        
        [Column("id_group")]
        public long? IdGroup { get; set; }
        
        [Column("id_user")]
        public long? IdUser { get; set; }

        [ForeignKey("IdUser")]
        public virtual User User { get; set; }
        [ForeignKey("IdGroup")]
        public virtual Group Group { get; set; }
    }
}