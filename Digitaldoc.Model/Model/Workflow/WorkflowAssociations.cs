﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Digitaldoc.Model.WF
{
    [Table("wf_flow_associations")]
    public class WorkflowAssociations : Registro
    {
        
        [Column("id_flow")]
        public long IdWorkflow { get; set; }
        
        [Column("id_document_type")]
        public long? IdDocumentType { get; set; }
        
        [Column("id_folder")]
        public long? IdFolder { get; set; }

        [ForeignKey("IdWorkflow")]
        public virtual Workflow Workflow { get; set; }

    }

    public class WAWorkflowComparer : IEqualityComparer<WorkflowAssociations>
    {
        public bool Equals(WorkflowAssociations x, WorkflowAssociations y)
        {
            return x.IdWorkflow == y.IdWorkflow;
        }

        public int GetHashCode(WorkflowAssociations obj)
        {
            return base.GetHashCode() * 5;
        }
    }
}