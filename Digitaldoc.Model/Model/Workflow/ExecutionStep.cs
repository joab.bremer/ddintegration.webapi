﻿using Digitaldoc.Model.WF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Workflow
{
    [Table("wf_execution_steps")]
    public class ExecutionStep : Registro
    {
        [Column("id_element")]
        public long IdElement { get; set; }
        [Column("id_execution")]
        public long IdExecution { get; set; }

        [Column("start_Date")]
        public DateTime? StartDate { get; set; }
        [Column("end_date")]
        public DateTime? EndDate { get; set; }

        [Column("row_guid")]
        public Guid RowGuid { get; set; }

        [ForeignKey("IdExecution")]
        public virtual Execution Execution { get; set; }

        [ForeignKey("IdElement")]
        public virtual Element Element { get; set; }

        [NotMapped]
        public int? RemainingDays { get; set; }

        [NotMapped]
        public DateTime? DateLimit { get; set; }
    }
}