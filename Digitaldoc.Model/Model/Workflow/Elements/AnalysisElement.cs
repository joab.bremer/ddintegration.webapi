﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    public class AnalysisElement : Element
    {

        public AnalysisElement()
        {
            Type = EElementType.Analysis;
        }

    }
}