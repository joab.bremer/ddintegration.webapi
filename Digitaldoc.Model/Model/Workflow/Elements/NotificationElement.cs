﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    public class NotificationElement : Element
    {

        public NotificationElement()
        {
            Type = EElementType.Notification;
        }

        [Column("message")]
        public string Message { get; set; }

        [Column("title")]
        public string Title
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;
            }
        }

    }
}