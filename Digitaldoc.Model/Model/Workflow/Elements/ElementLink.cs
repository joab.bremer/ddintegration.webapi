﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    [Table("wf_element_links")]
    public class ElementLink : Registro
    {

        [Column("id_element")]
        public long IdElement { get; set; }

        [Column("id_next_element")]
        public long IdNextElement { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("css_color")]
        public string CssColor { get; set; }

    }
}