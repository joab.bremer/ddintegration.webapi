﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    public class EndElement : Element
    {

        public EndElement()
        {
            Type = EElementType.End;
        }

        [Column("id_end_action")]
        public EEndAction EndAction { get; set; }

    }
}