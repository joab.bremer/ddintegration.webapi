﻿namespace Digitaldoc.Model.WF
{
    public class ApproveWithSignatureElement : Element
    {

        public ApproveWithSignatureElement()
        {
            Type = EElementType.ApproveWithSignature;
        }

    }
}