﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    [Table("wf_lane_elements")]

    public abstract class Element : Registro
    {

        [Column("name")]
        public string Name { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("joint_id")]
        public Guid JointId { get; set; }

        [Column("id_type")]
        public EElementType Type { get; set; }

        [Column("id_lane")]
        public long IdLane { get; set; }
        [Column("is_description_optional")]
        public bool IsDescriptionOptional { get; set; }

        [Column("duration")]
        public int? Duration { get; set; }

        [NotMapped]
        public List<Group> Groups { get; set; }

        [NotMapped]
        public List<User> Users { get; set; }

        [ForeignKey("IdLane")]
        public virtual Swimlane Lane { get; set; }

        public virtual List<ElementLink> Links { get; set; }

    }
}