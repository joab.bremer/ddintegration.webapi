﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    public class DistributionElement : Element
    {

        public DistributionElement()
        {
            Type = EElementType.Distribution;
        }

    }
}