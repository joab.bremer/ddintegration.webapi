﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Digitaldoc.Model.WF
{
    [Table("wf_element_persons")]
    public class ElementPerson : Registro
    {
        
        [Column("id_element")]
        public long IdElement { get; set; }
        
        [Column("id_group")]
        public long? IdGroup { get; set; }
        
        [Column("id_user")]
        public long? IdUser { get; set; }

        [ForeignKey("IdUser")]
        public virtual User User { get; set; }
        [ForeignKey("IdGroup")]
        public virtual Group Group { get; set; }

    }
}