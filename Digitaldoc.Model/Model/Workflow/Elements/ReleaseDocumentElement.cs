﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    public class ReleaseDocumentElement : Element
    {

        public ReleaseDocumentElement()
        {
            Name = "RELEASE DOCUMENT";
            Type = EElementType.ReleaseDocument;
        }

    }
}