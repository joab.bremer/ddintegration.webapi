﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    public class DecisionElement : Element
    {

        public DecisionElement()
        {
            Type = EElementType.Decision;
        }

    }
}