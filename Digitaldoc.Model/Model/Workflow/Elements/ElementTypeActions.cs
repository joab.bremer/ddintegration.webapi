﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    [Table("wf_element_type_actions")]
    public class ElementTypeActions
    {

        [Column("id_action")]
        public EStepAction Action { get; set; }

        [Column("id_element_type")]
        public EElementType ElementType { get; set; }

        [Column("is_required")]
        public bool IsRequired { get; set; }

    }
}