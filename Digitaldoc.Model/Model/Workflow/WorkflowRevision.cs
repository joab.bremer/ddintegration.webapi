﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    [Table("wf_flow_revisions")]
    public class WorkflowRevision : Registro
    {

        [Column("id_flow")]
        public long IdWorkflow { get; set; }

        [Column("number")]
        public int Number { get; set; }

        [Column("json_graph")]
        public string GraphElements { get; set; }

        [Column("creation_date")]
        public DateTime CreationDate { get; set; }

        [Column("removed")]
        public bool Removed { get; set; }

        [ForeignKey("IdWorkflow")]
        public virtual Workflow Workflow { get; set; }
        
    }
}