﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    [Table("wf_lanes")]
    public class Swimlane : Registro
    {

        [Column("name")]
        public string Name { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("joint_id")]
        public Guid JointId { get; set; }

        [Column("id_flow_revision")]
        public long IdWorkflowRevision { get; set; }

        [Column("person_document_author")]
        public bool? PersonDocumentAuthor { get; set; }

        [NotMapped]
        public List<User> Users { get; set; }

        [NotMapped]
        public List<Group> Groups { get; set; }
    }
}