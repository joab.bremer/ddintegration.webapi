﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("configurations")]
    public class Configuration : Registro
    {
        [Column("id_company")]
        public long IdCompany { get; set; }
        [Column("is_description_revision_required")]
        public bool IsDescriptionRevisionRequired {get;set;}
        [Column("delete_files")]
        public bool DeleteFiles { get; set; }
        [Column("sign_pdf_as_p7s")]
        public bool SignPdfAsP7s { get; set; }
        [Column("is_workflow_optional")]
        public bool IsWorkflowOptional { get; set; }
        [Column("show_only_approved_revisions")]
        public bool ShowOnlyApprovedRevisions { get; set; }
        [Column("code_format")]
        public string CodeFormat { get; set; }
        [Column("value0")]
        public int Value0 { get; set; }
        [Column("value1")]
        public int Value1 { get; set; }
        [Column("value2")]
        public int Value2 { get; set; }
        [Column("value3")]
        public int Value3 { get; set; }
        [Column("value4")]
        public int Value4 { get; set; }
        [Column("value5")]
        public int Value5 { get; set; }

        [Column("replace_generate_new_revision")]
        public bool GenerateNewRevisionOnReplaceFile { get; set; }

    }
}