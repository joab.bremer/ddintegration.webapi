﻿using System.Collections.Generic;

namespace Digitaldoc.Model
{
    public class LinkedRevisions
    {

        public long IdMainRevision { get; set; }

        public List<long> IdsLinkedRevisions { get; set; }

        public Revision MainRevision { get; set; }

        public List<Revision> ListLinkedRevisions { get; set; }

    }
}