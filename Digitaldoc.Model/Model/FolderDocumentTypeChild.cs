﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Digitaldoc.Model
{
    [Table("folder_doctype_children")]
    public class FolderDocumentTypeChild
    {
        [Key]
        [Column("id_folder_preference", Order = 0)]
        public long IdFolderPreference { get; set; }

        [Key]
        [Column("id_document_type", Order = 1)]
        public long IdDocumentType { get; set; }
        
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return ((FolderDocumentTypeChild)obj).IdFolderPreference == this.IdFolderPreference && ((FolderDocumentTypeChild)obj).IdDocumentType == this.IdDocumentType;

        }
        
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}