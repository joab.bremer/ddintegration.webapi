﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("user_admin_features")]
    public class UserAdminFeatures
    {
        [Key]
        [Column("id_user", Order = 1)]
        public long IdUser { get; set; }

        [Key]
        [Column("id_admin_feature", Order = 2)]
        public EAdminFeatures IdAdminFeature { get; set; }


        public override bool Equals(object obj)
        {
            if (obj is UserAdminFeatures)
            {
                var item = (UserAdminFeatures)obj;

                if (item.IdAdminFeature == IdAdminFeature && item.IdUser == IdUser)
                {
                    return true;
                }
            }

                return false;
        }

        public override int GetHashCode()
        {
            return (int) (((IdUser + (int)IdAdminFeature) * 2) + IdUser);
        }
    }
}