﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("additional_fields")]
    public class AdditionalField : Registro
    {       

        [Column("id_company")]
        public long IdCompany { get; set; }

        [Column("id_field_type")]
        public int IdFieldType { get; set; }

        [Column("id_list")]
        public long? IdList{ get; set; }
                
        [Column("validation_type")]
        public EAdditionalFieldValidationType ValidationType { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Removed { get; set; }

        [ForeignKey("IdList")]
        public virtual DataList List { get; set; }

        [ForeignKey("IdFieldType")]
        public AdditionalFieldType AdditionalFieldType { get; set; }
               
    }
}