﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Digitaldoc.Model.Integration;
using System.ComponentModel;
using Digitaldoc.Model.Enums;

namespace Digitaldoc.Model
{
    [Table("revisions")]
    public class Revision : Registro
    {
        public Revision()
        {

        }

        public Revision(RevisionIntegration ri)
        {
            Id = ri.Id;
            Extension = ri.Extension;
            Code = ri.Code;
            Date = ri.Date;
            Description = ri.Description;
            Keywords = ri.KeyWords;
            IdFolder = ri.IdFolder ?? 0;
            IdDocumentType = ri.IdDocumentType ?? 0;
            Size = ri.Size;
            Title = ri.Title.Replace("–", "-");
            RevisionNumber = ri.RevisionNumber;
            if (ri.AdditionalFields != null)
            {
                var fields = new List <RevisionAdditionalField>();
                ri.AdditionalFields.ForEach(a => fields.Add(new RevisionAdditionalField(a)));
                AdditionalFields = fields;
            }
        }

        [Column("id_author")]
        public long IdAuthor { get; set; }
        [Column("id_folder")]
        public long IdFolder { get; set; }
        [Column("id_document_type")]
        public long IdDocumentType { get; set; }
        [Column("id_status")]
        public int IdStatus { get; set; }
        [Column("id_company")]
        public long IdCompany { get; set; }
        [Column("trash_id_user")]
        public long? IdTrashUser { get; set; }

        [DefaultValue(ERevisionStatus.Approved)]
        [Column("revision_status")]
        public ERevisionStatus RevisionStatus { get; set; }

        public string Code { get; set; }
        public DateTime Date { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        [Column("number_pages")]
        public int? NumberOfPages { get; set; }
        public int Removed { get; set; }
        public string Extension { get; set; }
        public long Size { get; set; }
        [Column("revision")]
        public int RevisionNumber { get; set; }
        [Column("revision_max_number")]
        public int RevisionMaxNumber { get; set; }
        [Column("next_revision_date")]
        public DateTime? NextRevisionDate { get; set; }

        [DataType(DataType.Date)]
        [Column("revision_notification_date", TypeName = "Date")]
        public DateTime? RevisionNotificationDate { get; set; }
        [Column("notification_days")]
        public int? NotificationDays { get; set; }
        [Column("trash_date")]
        public DateTime? TrashDate { get; set; }
        [Column("content_indexed")]
        public bool ContentIndexed { get; set; }
        [Column("properties_indexed")]
        public bool PropertiesIndexed { get; set; }

        [Column("integration")]
        public EIntegration Integration { get; set; }

        [Column("notified")]
        public bool Notified { get; set; }

        [Column("is_public")]
        public bool IsPublic { get; set; }

        [Column("signed")]
        public bool IsSigned { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("row_guid")]
        public Guid RowGuid { get; set; }

        [NotMapped]
        public byte[] FileData { get; set; }

        [ForeignKey("IdTrashUser")]
        public virtual User TrashUser { get; set; }

        [ForeignKey("IdAuthor")]
        public virtual User Author { get; set; }

        [ForeignKey("IdFolder")]
        public virtual Folder Folder { get; set; }

        [NotMapped]
        public RevisionCheckout RevisionCheckout { get; set; }

        [ForeignKey("IdDocumentType")]
        public virtual DocumentType DocumentType { get; set; }

        //cuidado ao retirar essa anotação, vai impactar no salvar
        //se for mexer aqui altere no salvar tbm
        
        public virtual List<RevisionAdditionalField> AdditionalFields { get; set; }

        #region Campos da pesquisa
        /// <summary>
        /// Propriedade para fazer o agrupamento dos resultados na tela
        /// </summary>
        [NotMapped]
        public String GroupBy { get; set; }
        /// <summary>
        /// Em uma busca o termo pesquisado pode ser encontrado em um ou mais campos.
        /// Esse dicionario guarda a chave (campo onde foi encontrado o termo) e valor (conteúdo com o highlight para o campo)
        /// </summary>
        [NotMapped]
        public Dictionary<String, List<String>> HighlightFields { get; set; }

        [NotMapped]
        public List<int> Features { get; set; }

        #endregion

        #region Campos Migração

        [Column("id_migrated_revision")]
        public int? IdMigrateRevision { get; set; }

        #endregion

        [NotMapped]
        public User SharedBy { get; set; }
       
        [NotMapped]
        public bool IsFavorite { get; set; }
        [NotMapped]
        public bool HasLinkedDocuments { get; set; }

        [NotMapped]
        public bool IsHistory { get; set; }

        [NotMapped]
        public long? IdRevisionHistory { get; set; }

        /// <summary>
        /// Retorna a extensão original antes de assinar. Ex: um doc.p7s, retornda apenas doc
        /// </summary>
        [NotMapped]
        public string ExtensionNotSigned
        {
            get
            {
                if (IsSigned && Extension.ToLower().EndsWith("p7s"))
                {
                    return Extension.Split('.').FirstOrDefault();
                }
                return Extension;
            }
        }

        [NotMapped]
        public bool IsP7s
        {
            get
            {
                return IsSigned && Extension.ToLower().EndsWith("p7s");
            }
        }

        [NotMapped]
        public string CurrentStepName { get; set; }
    }
}