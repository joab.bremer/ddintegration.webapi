﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("users")]
    public class User : Registro, IIdentity
    {
        [Column("id_preference")]
        public long IdPreference { get; set; }

        [MaxLength(200)]
        public string Name { get; set; }
        [RegularExpression(@"^[0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2}$")]
        public string CPF { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Pass { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Column("change_pass")]
        public bool ChangePass { get; set; }

        [Column("is_sysadmin")]
        public bool IsSysAdmin { get; set; }

        [Column("is_bloqued")]
        public bool IsBloqued { get; set; }

        [Column("is_autologin")]
        public bool IsAutoLogin { get; set; }

        [Column("autologin_token")]
        public String AutologinToken { get; set; }

        [Column("id_folder_integration")]
        public long? IdFolderIntegration { get; set; }

        public int Removed { get; set; }

        [NotMapped]
        public Company ActiveCompany { get; set; }

        public string AuthenticationType
        {
            get { return ""; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }

        public virtual List<GroupUser> Groups { get; set; }

        public List<UserAdminFeatures> AdminFeatures { get; set; }

        public virtual List<UserCompany> Companies { get; set; }

        [ForeignKey("IdPreference")]
        public virtual UserPreferences UserPreferences { get; set; }

        [ForeignKey("IdFolderIntegration")]
        public virtual Folder Folder { get; set; }
    }
}