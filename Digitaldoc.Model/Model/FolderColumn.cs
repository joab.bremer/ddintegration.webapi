﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("folder_columns")]
    public class FolderColumn
    {

        [Key]
        [Column("id_folder_preference", Order = 0)]
        public long IdFolderPreference { get; set; }

        [Key]
        [Column("column_name", Order = 1)]
        public string ColumnName { get; set; }

        // override object.Equals
        public override bool Equals(object obj)
        {

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return this.GetHashCode() == ((FolderColumn)obj).GetHashCode() && this.IdFolderPreference == ((FolderColumn)obj).IdFolderPreference 
                && this.SortDirection == ((FolderColumn)obj).SortDirection && this.Grouping == ((FolderColumn)obj).Grouping
                && this.GroupOrder == ((FolderColumn)obj).GroupOrder && this.Visible == ((FolderColumn)obj).Visible;

        }

        [Column("sort_direction")]
        public int SortDirection { get; set; }

        [Column("grouping")]
        public bool Grouping { get; set; }

        [Column("group_order")]
        public int GroupOrder { get; set; }

        [Column("visible")]
        public bool Visible { get; set; }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return this.ColumnName.GetHashCode() * 17;
        }
        
    }
}