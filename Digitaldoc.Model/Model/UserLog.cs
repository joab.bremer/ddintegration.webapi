﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("user_logs")]
    public class UserLog : Registro
    {
        [Column("id_user")]
        public long? IdUser { get; set; }
        [Column("id_folder")]
        public long? IdFolder { get; set; }
        [Column("id_company")]
        public long? IdCompany { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        [Column("execution_duration")]
        public double? ExecutionDuration { get; set; }
        [Column("number_of_results")]
        public int? NumberOfResults { get; set; }
        [Column("search_query")]
        public string SearchQuery { get; set; }
        [Column("search_fields")]
        public string SearchFields { get; set; }
        [Column("search_folder_filter")]
        public string SearchFolderFilter { get; set; }
        [Column("url_called")]
        public string UrlCalled { get; set; }
        [Column("user_agent")]
        public string UserAgent { get; set; }
        public string Exception { get; set; }

        /// <summary>
        /// a propriedade Date já é setada quando instancia
        /// não é necessário setar ela novamente
        /// </summary>
        public UserLog()
        {
            Date = DateTime.Now;
        }

    }
}