﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("folders_document_types_view")]
    public class FolderDocumentTypeView
    {
        private List<long> _documentTypesIds;

        [Key]
        [Column("id")]
        public long IdFolder { get; set; }
        [Column("id_parent")]
        public long? IdParent { get; set; }
        [Column("id_document_type")]
        public string IdsDocumentTypes { get; set; }

        public List<long> DocumentTypesIds
        {
            get
            {
                _documentTypesIds = new List<long>();
                if (IdsDocumentTypes != null)
                {
                    IdsDocumentTypes.Split(',').ToList().ForEach(c => _documentTypesIds.Add(Convert.ToInt64(c.Trim())));
                }
                return _documentTypesIds;
            }
        }
    }
}