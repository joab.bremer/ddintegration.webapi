﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("folder_preferences")]
    public class FolderPreference : Registro
    {
        [Column("id_user")]
        public long IdUser { get; set; }

        [Column("id_folder")]
        public long IdFolder { get; set; }

        [Column("id_additional_field")]
        public long? IdAdditionalField { get; set; }

        [Column("id_document_type")]
        public long? IdDocumentType { get; set; }

        [NotMapped]
        public List<FolderColumn> FolderColumns { get; set; }

        [NotMapped]
        [ForeignKey("IdUser")]
        public User User { get; set; }

        [NotMapped]
        [ForeignKey("IdFolder")]
        public Folder Folder { get; set; }

    }
}