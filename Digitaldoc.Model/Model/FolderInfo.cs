﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public class FolderInfo
    {
        public string Name { get; set; }
        public int FilesCount { get; set; }
        public int ChildrenCount { get; set; }
    }
}