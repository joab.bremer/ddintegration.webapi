﻿using Digitaldoc.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Model.Reports
{
    public class AddFieldReportsFilter
    {

        public EReportDateFilter DateFilterType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public AdditionalField AddField { get; set; }

        public EReportType ReportType { get; set; }

        public AdditionalFieldType AddFieldType { get; set; }

        public string Value { get; set; }

        public EReportTextFilter TextFilter { get; set; }

        public EReportNumberFilter NumberFilter { get; set; }
        
        





    }
}