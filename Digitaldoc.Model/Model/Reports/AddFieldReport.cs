﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Model.Reports
{
    public class AddFieldReport
    {
        public string FolderPath{ get; set; }

        public string TitleDocument { get; set; }

        public string AddField { get; set; }

        public string Value { get; set; }
    }
}