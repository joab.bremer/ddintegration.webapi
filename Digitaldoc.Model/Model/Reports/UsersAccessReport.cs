﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public class UsersAccessReport
    {

        public long IdUser { get; set; }

        public long IdCompany { get; set; }

        public DateTime? Date { get; set; }

        public string Email { get; set; }

    }
}