﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Model.Reports
{
    public class ExpireDocumentReport
    {
        public string FolderPath { get; set; }

        public string TitleDocument { get; set; }

        public string Code { get; set; }

        public DateTime ExpireDate { get; set; }
    }
}