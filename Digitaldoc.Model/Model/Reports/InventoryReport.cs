﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Model.Reports
{
    public class InventoryReport
    {
        public string FolderPath { get; set; }

        public string TitleDocument { get; set; }

        public string UserName { get; set; }

        public DateTime InsertionDate { get; set; }
    }
}