﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Reports
{
    public class WorkflowRunReport
    {

        public long IdExecutionStep { get; set; }
        public string StepName { get; set; }
        public string UserName { get; set; }
        public string Date { get; set; }
        public string Comments { get; set; }
        public string Element { get; set; }
        
        public string StepDescription { get; set; }
    }
}