﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public class IndexedPagesReport
    {

        public string FolderName { get; set; }

        public string UserName { get; set; }

        public int TotalPagesIndexed { get; set; }

    }
}