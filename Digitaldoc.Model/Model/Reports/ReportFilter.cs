﻿using Digitaldoc.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public class ReportFilter
    {

        public EReportDateFilter DateFilterType { get; set; }

        public DateTime? StartDate { get; set; }

        public bool NoDate { get; set; }

        public DateTime? EndDate { get; set; }

        public List<long> CompaniesIds { get; set; }

        public long IdUser { get; set; }

        public long IdDocumentType { get; set; }

        public EReportType ReportType { get; set; }

        public Guid StepId { get; set; }


        public long IdAddField { get; set; }

        public long IdAddFieldType { get; set; }

        public string Value { get; set; }

        public EReportTextFilter TextFilter { get; set; }

        public EReportNumberFilter NumberFilter { get; set; }

        public string AddFieldName { get; set; }

        public bool NoFilter { get; set; }
    }
}