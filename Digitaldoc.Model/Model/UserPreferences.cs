﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("user_preferences")]
    public class UserPreferences : Registro
    {
        [Column("id_last_company")]
        public long IdLastCompany { get; set; }

        [Column("id_last_folder")]
        public long? IdLastFolder { get; set; }

        [Column("show_preview_pending_scans")]
        public bool ShowPreviewPendingScans { get; set; }

        [Column("show_preview_send_files")]
        public bool ShowPreviewOnSendFiles { get; set; }

        [Column("search_fields")]
        public string SearchFields { get; set; }

        [Column("search_on_content")]
        public bool? SearchOnContent { get; set; }

        [Column("last_search_terms")]
        public string LastSearchTerms { get; set; }

        [Column("dont_show_message_p7s_again")]
        public bool DontShowMessageP7sAgain { get; set; }
    }
}