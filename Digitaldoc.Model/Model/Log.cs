﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Model
{
    [Table("logs")]
    public class Log
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Column("id_user")]
        public long? IdUser { get; set; }
        [Column("id_company")]
        public long? IdCompany { get; set; }
        [Column("id_folder")]
        public long? IdFolder { get; set; }
        [Column("id_revision")]
        public long? IdRevision { get; set; }
        public DateTime Date { get; set; }
        public string Url { get; set; }
        public string Message { get; set; }
    }
}