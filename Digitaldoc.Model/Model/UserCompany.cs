﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("user_companies")]
    public class UserCompany
    {
        [Key]
        [Column("id_user", Order = 0)]
        public long IdUser { get; set; }

        [Key]
        [Column("id_company", Order = 1)]
        public long IdCompany { get; set; }

        [Column("is_admin", Order = 2)]
        public bool IsAdmin { get; set; }

        [ForeignKey("IdCompany")]
        public virtual Company Company { get; set; }
    }
}