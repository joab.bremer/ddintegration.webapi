﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public class TreeItem
    {
        [Required]
        public long Id { get; set; }
        public long? IdParent { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public TreeItemType TreeItemType { get; set; }

        public bool CanSendFiles { get; set; }
        public int FileCount { get; set; }
        public bool HasChildren { get; set; }

        public bool HasPermission { get; set; }
        public bool CanManageFolders { get; set; }
        public string Features { get; set; }

        public IEnumerable<int> GetFeaturesEnumerable()
        {
            if (!String.IsNullOrEmpty(Features))
            {
                return Features.Split(',').Select(c => int.Parse(c)).ToList();
            }
            return new List<int>();
        }

        public override bool Equals(object obj)
        {
            if (obj is TreeItem)
            {
                var e = (TreeItem)obj;

                if (e.Id == Id && e.IdParent == IdParent && e.Name == Name && e.Description == Description)
                {
                    return true;
                }
            }

            return false;
        }
    }

    public class TreeItemEqualityComparer : EqualityComparer<TreeItem>
    {
        public override bool Equals(TreeItem x, TreeItem y)
        {
            if (x.TreeItemType == TreeItemType.AdditionalField)
            {
                return x.Id == y.Id && x.Name == y.Name;
            }
            return x.Id == y.Id;
        }

        public override int GetHashCode(TreeItem obj)
        {
            return obj.Id.GetHashCode() ^ obj.IdParent.GetHashCode();
        }
    }

    public enum TreeItemType
    {
        //qualquer alteração no valor desses enums deve ser verificado a SQL na classe TreeViewRepository
        Repository = 0,
        Folder = 1,
        DocumentType = 2,
        AdditionalField = 3,
        Loading = 4,
        QuickAccess = 5,
        SharedDocuments = 6,
        PendingScans = 7,
        Checkouts = 8,
        TrashCan = 9,
        Favorites = 10,
    }
}