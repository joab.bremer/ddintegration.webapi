﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public class GetTreeItemModel
    {
        public TreeItem Item{ get; set; }
        public int startIndex { get; set; }
        public int pageSize { get; set; }
    }

    public class RevisionVirtualizingResult
    {
        public int count;
        public List<Revision> result;

        public RevisionVirtualizingResult(int count, List<Revision> result)
        {
            // TODO: Complete member initialization
            this.count = count;
            this.result = result;
        }
    }
}