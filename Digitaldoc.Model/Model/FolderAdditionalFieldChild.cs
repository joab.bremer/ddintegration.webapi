﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Digitaldoc.Model
{
    [Table("folder_addfield_children")]
    public class FolderAdditionalFieldChild
    {
        [Key]
        [Column("id_folder_preference", Order = 0)]
        public long IdFolderPreference { get; set; }

        [Key]
        [Column("id_additional_field", Order = 1)]
        public long IdAdditionalField { get; set; }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return ((FolderAdditionalFieldChild)obj).IdFolderPreference == this.IdFolderPreference && ((FolderAdditionalFieldChild)obj).IdAdditionalField == this.IdAdditionalField;
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}