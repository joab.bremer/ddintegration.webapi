﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("shared_revisions")]
    public class SharedRevision : IEquatable<SharedRevision>
    {
        [Key]
        [Column("id_revision", Order = 0)]
        public long IdRevision { get; set; }
        [Key]
        [Column("id_user_sender", Order = 1)]
        public long IdUserSender { get; set; }
        [Key]
        [Column("id_user_receiver", Order = 2)]
        public long IdUserReceiver { get; set; }
        [Key]
        [Column("id_role", Order = 3)]
        public long IdRole { get; set; }
        [Column("shared_date")]
        public DateTime SharedDate { get; set; }

        [NotMapped]
        public long IdFolder { get; set; }

        [NotMapped]
        public string SharedBy { get; set; }

        [ForeignKey("IdRevision")]
        public virtual Revision Revision { get; set; }

        [ForeignKey("IdUserSender")]
        public virtual User Sender { get; set; }

        [ForeignKey("IdUserReceiver")]
        public virtual User Receiver { get; set; }

        [ForeignKey("IdRole")]
        public virtual Role Role { get; set; }

        public bool Equals(SharedRevision other)
        {
            if (Object.ReferenceEquals(other, null))
            {
                return false;
            }

            if(Object.ReferenceEquals(this, other))
            {
                return true;
            }

            return this.IdRevision == other.IdRevision && this.IdRole == other.IdRole && this.IdUserReceiver == other.IdUserReceiver;

        }
    }
}