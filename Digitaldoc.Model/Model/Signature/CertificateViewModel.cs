﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public class CertificateViewModel
    {
        public string Name { get; set; }
        public DateTime EffectiveDateString { get; set; }
        public DateTime ExpirationDateString { get; set; }

        public DateTime? SignatureDate { get; set; }
    }
}