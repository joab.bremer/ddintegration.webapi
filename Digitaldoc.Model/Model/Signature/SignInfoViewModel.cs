﻿using System;
using System.Linq;

namespace Digitaldoc.Model.Model.Signature
{
    public class SignInfoViewModel
    {
        public string FileHash { get; set; }

        public void SetFileHashBytes(byte[] value)
        {
            FileHash = BitConverter.ToString(value).Replace("-", string.Empty);
        }

        public byte[] FileHashBytes
        {
            get
            {
                return Enumerable.Range(0, FileHash.Length)
                     .Where(x => x % 2 == 0)
                     .Select(x => Convert.ToByte(FileHash.Substring(x, 2), 16))
                     .ToArray();
            }
        }

        public string FieldName { get; set; }
    }
}