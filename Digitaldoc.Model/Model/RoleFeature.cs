﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("roles_features")]
    public class RoleFeature
    {
        [Key]
        [Column("id_feature",Order = 1)]
        public int IdFeature { get; set; }
        [Key]
        [Column("id_role", Order = 2)]
        public long IdRole { get; set; }

        [ForeignKey("IdFeature")]
        public virtual Feature Feature { get; set; }

        public override bool Equals(object obj)
        {

            if (this == null && obj == null)
            {
                return true;
            }

            var r = obj as RoleFeature;

            if (r == null)
            {
                return false;
            }

            if (r.IdFeature != this.IdFeature || r.IdRole != this.IdRole)
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            if (IdFeature == 0 && IdRole == 0)
            {
                return 0;
            }

            return (int) ((IdFeature + IdRole) * Math.PI) + 112;
        }
    }
}