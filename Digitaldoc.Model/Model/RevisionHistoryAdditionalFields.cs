﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("revision_history_additional_fields")]
    public class RevisionHistoryAdditionalFields
    {
        [Key]
        [Column("id_revision_history", Order = 0)]
        public long IdRevisionHistory { get; set; }
        [Key]
        [Column("id_additional_field", Order = 1)]
        public long IdAdditionalField { get; set; }
        public string Value { get; set; }

        [ForeignKey("IdAdditionalField")]
        public virtual AdditionalField AdditionalField { get; set; }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            var other = (RevisionHistoryAdditionalFields)obj;
            return this.IdRevisionHistory == other.IdRevisionHistory && this.IdAdditionalField == other.IdAdditionalField;
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return (int) ((this.IdAdditionalField + this.IdRevisionHistory) * 2 + this.IdAdditionalField);
        }

    }
}