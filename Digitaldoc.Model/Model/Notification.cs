﻿using Digitaldoc.Model.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("notifications")]
    public class Notification : Registro 
    {
        [Column("id_revision")]
        public long? IdRevision { get; set; }

        [Column("id_user_target")]
        public long? IdUserTarget { get; set; }

        [Column("id_user_sender")]
        public long? IdUserSender { get; set; }

        [Column("id_company")]
        public long IdCompany { get; set; }

        [Column("notification_feature")]
        public ENotificationFeature NotificationFeature { get; set; }

        [Column("type")]
        public ENotificationType Type { get; set; }

        [Column("creation_date")]
        public DateTime CreationDate { get; set; }

        [Column("email_sent")]
        public bool EmailSent { get; set; }

        [Column("email_sent_date")]
        public DateTime? EmailSentDate { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("text")]
        public string Text { get; set; }
        
    }
}