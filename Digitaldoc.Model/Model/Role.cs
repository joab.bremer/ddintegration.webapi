﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("roles")]
    public class Role : Registro
    {    
        [Column("id_company")]
        public long IdCompany { get; set; }
        
        [Required(AllowEmptyStrings = false)]        
        public string Name { get; set; }
        public string Description { get; set; }

        [Column("removed")]
        public bool Removed { get; set; }

        public virtual List<RoleFeature> Features { get; set; }
    }
}