﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("companies")]
    public class Company : Registro
    {
        public Company()
        {
            Users = new List<User>();
        }

        [Column("id_status")]
        public int IdStatus { get; set; }
        [Column("id_contract")]
        public long IdContract { get; set; }

        [Required]
        public string Name { get; set; }
        [RegularExpression(@"^[0-9]{2}.[0-9]{3}.[0-9]{3}/[0-9]{4}-[0-9]{2}$", ErrorMessage = "O formato de CNPJ não é válido")]
        public string Cnpj { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        [Column("zip_code")]
        [DataType(DataType.PostalCode)]
        public string ZipCode { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Formato de Email inválido")]
        public string Email { get; set; }

        [Column("contact_person")]
        [Required]
        public string ContactPerson { get; set; }

        [NotMapped]
        public byte[] Logo { get; set; }

        [NotMapped]
        public string LogoBase64 { get; set; }

        [Column("logo_filename")]
        public string LogoFilename { get; set; }
        public int Removed { get; set; }
        public string Segment { get; set; }

        [Column("hasautologin")]
        public bool HasAutoLogin { get; set; }

        [Column("can_export_media")]
        public bool CanExportMedia { get; set; }

        [Column("registration_date")]
        public DateTime RegistrationDate { get; set; }

        [Column("can_scheduled_send")]
        public bool CanScheduledSend { get; set; }
        [Column("can_sign_files")]
        public bool CanSignFiles { get; set; }
        [Column("can_workflow")]
        public bool CanWorkflow { get; set; }

        [Column("can_nfe")]
        public bool CanNFe { get; set; }

		[Column("certificate")]
		public byte[] Certificate { get; set; }

        [Column("certificate_serial_number")]
        public string CertificateSerialNumber { get; set; }

        [Column("certificate_password")]
        public string CertificatePassword { get; set; }

        [Column("legal_name")]
        public string LegalName { get; set; }

        [Column("autocomplete_af")]
        public bool AutoCompleteAf { get; set; }
        
        [ForeignKey("IdContract")]
        public virtual Contract Contract { get; set; }


        [NotMapped]
        public virtual ICollection<User> Users { get; set; }

        [NotMapped]
        public int UserCount { get; set; }

	}
}