﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Model.Tasy
{
    public class TasyFilter
    {
        public string DocumentsType { get; set; }

        public string ExamsType { get; set; }

        public string PatientName { get; set; }

        public string MedicalRecord { get; set; }

        public string AppointmentNumber { get; set; }
        
        public bool SelectedExams { get; set; }

       

    }
}