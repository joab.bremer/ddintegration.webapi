﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Model
{
    [Table("company_autocompletes")]
    public class CompanyAutoComplete
    {
        [Key]
        [Column("id_company", Order = 1)]
        public long IdCompany { get; set; }
        [Key]
        [Column("id_additional_field", Order=2)]
        public long IdAdditionalField { get; set; }

        [Column("isMain")]
        public bool IsMain { get; set; }

        [ForeignKey("IdCompany")]
        public virtual Company Company { get; set; }

        [ForeignKey("IdAdditionalField")]
        public virtual AdditionalField AdditionalField { get; set; }

    }
}