﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("contracts")]
    public class Contract : Registro
    {
        [Column("start_date")]
        public DateTime StartDate { get; set; }
        [Column("company_name")]
        public string CompanyName { get; set; }
        [Column("cnpj")]
        public string CNPJ { get; set; }
        [Column("contract_responsible")]
        public string ContractResponsible { get; set; }
        [Column("responsible_seller")]
        public string ResponsibleSeller { get; set; }
        [Column("gbs_limit")]
        public int GbsLimit {get;set;}
        [Column("users_limit")]
        public int UsersLimit { get; set; }
        [Column("id_contract_status")]
        public EContractStatus IdContractStatus { get; set; }
        [Column("removed")]
        public bool Removed { get; set; }

        [Column("summed_size")]
        public long? SummedSize { get; set; }
        
        [NotMapped]
        public virtual ICollection<Company> Companies { get; set; }
        [NotMapped]
        public int UserCount { get; set; }
        [NotMapped]
        public decimal? SummedSizeInGbs
        {
            get
            {
                if (SummedSize == null || SummedSize == 0)
                {
                    return null;
                }

                var kb = decimal.Divide((decimal)SummedSize, 1000);
                var mb = decimal.Divide(kb, 1000);
                var gb = decimal.Divide(mb, 1000);

                return gb;
            }
        }
            
    }
}