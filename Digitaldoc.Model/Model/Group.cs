﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("groups")]
    public class Group : Registro
    {
        [Column("id_company")]
        public long IdCompany { get; set; }

        [Required]
        [MaxLength(255, ErrorMessage = "Tamanho máximo excedido.")]
        public string Name { get; set; }

        public virtual List<GroupFolderRole> FoldersRoles { get; set; }

        public virtual List<GroupUser> Users { get; set; }
    }
}