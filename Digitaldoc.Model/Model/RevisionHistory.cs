﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("revision_histories")]
    public class RevisionHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Column("id_folder")]
        public long IdFolder { get; set; }
        [Column("id_document_type")]
        public long IdDocumentType { get; set; }
        [Column("id_revision")]
        public long IdRevision { get; set; }
        [Column("id_author")]
        public long IdAuthor { get; set; }
        [Column("id_company")]
        public long IdCompany { get; set; }
        [Column("id_status")]
        public int IdStatus { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public DateTime Date { get; set; }
        [Column("number_pages")]
        public int? NumberOfPages { get; set; }
        public string Extension { get; set; }
        public long Size { get; set; }
        [Column("revision")]
        public int RevisionNumber { get; set; }
        [Column("next_revision_date")]
        public DateTime? NextRevisionDate { get; set; }

        [DataType(DataType.Date)]
        [Column("revision_notification_date", TypeName = "Date")]
        public DateTime? RevisionNotificationDate { get; set; }
        [Column("notification_days")]
        public int? NotificationDays { get; set; }
        [Column("revision_status")]
        public ERevisionStatus RevisionStatus { get; set; }
        [Column("signed")]
        public bool IsSigned { get; set; }
        public int Removed { get; set; }

        [NotMapped]
        [Column("file_data")]
        public byte[] FileData { get; set; }

        [NotMapped]
        public List<int> Features { get; set; }

        [ForeignKey("IdAuthor")]
        public virtual User Author { get; set; }

        [ForeignKey("IdFolder")]
        public virtual Folder Folder { get; set; }

        [ForeignKey("IdDocumentType")]
        public virtual DocumentType DocumentType { get; set; }

        //cuidado ao retirar essa anotação, vai impactar no salvar
        //se for mexer aqui altere no salvar tbm

        public virtual List<RevisionAdditionalField> AdditionalFields { get; set; }

        [Column("id_migrated_revision")]
        public int? IdMigrateRevision { get; set; }

    }

    public static class RevisionHistoryExtension
    {

        public static Revision ToRevision(this RevisionHistory hist)
        {
            return new Revision()
            {
                Id = hist.IdRevision,
                IsHistory = true,
                IdRevisionHistory = hist.Id,
                IdAuthor = hist.IdAuthor,
                IdCompany = hist.IdCompany,
                IdDocumentType = hist.IdDocumentType,
                IdFolder = hist.IdFolder,
                IdStatus = hist.IdStatus,
                Code = hist.Code,
                Author = hist.Author,
                AdditionalFields = hist.AdditionalFields,
                Date = hist.Date,
                Description = hist.Description,
                DocumentType = hist.DocumentType,
                Extension = hist.Extension,
                FileData = hist.FileData,
                Folder = hist.Folder,
                IsSigned = hist.IsSigned,
                Keywords = hist.Keywords,
                NextRevisionDate = hist.NextRevisionDate,
                NotificationDays = hist.NotificationDays,
                NumberOfPages = hist.NumberOfPages,
                Removed = hist.Removed,
                RevisionStatus = hist.RevisionStatus,
                Size = hist.Size,
                Title = hist.Title,
                RevisionNumber = hist.RevisionNumber,
                RevisionNotificationDate = hist.RevisionNotificationDate
            };
        }

    }
}