﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("groups_users")]
    public class GroupUser
    {
        [Key]
        [Column("id_user", Order = 0)]
        public long IdUser { get; set; }

        [Key]
        [Column("id_group", Order = 1)]
        public long IdGroup { get; set; }
    }
}