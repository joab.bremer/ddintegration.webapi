﻿using Digitaldoc.Model.Integration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    [Table("revision_additional_fields")]
    public class RevisionAdditionalField
    {
        public RevisionAdditionalField()
        {
        }

        public RevisionAdditionalField(RevisionAdditionalFieldsIntegration rafi)
        {
            IdAdditionalField = rafi.Id ?? 0;
            Value = rafi.Value;            
        }

        [Key]
        [Column("id_revision", Order = 1)]
        public long IdRevision { get; set; }

        [Key]
        [Column("id_additional_field", Order = 2)]
        public long IdAdditionalField { get; set; }

        public string Value { get; set; }

        [ForeignKey("IdAdditionalField")]
        public virtual AdditionalField AdditionalField { get; set; }

        [ForeignKey("IdRevision")]
        public virtual Revision Revision { get; set; }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            var other = (RevisionAdditionalField)obj;
            return this.IdRevision == other.IdRevision && this.IdAdditionalField == other.IdAdditionalField && this.Value == other.Value;
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return (this.IdAdditionalField + this.IdRevision).GetHashCode();
        }
    }
}