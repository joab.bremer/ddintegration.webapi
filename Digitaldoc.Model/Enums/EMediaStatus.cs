﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public enum EMediaStatus
    {
        Waiting = 0,
        Started = 1,
        Finished = 2,
        Error = 3,
    }
}