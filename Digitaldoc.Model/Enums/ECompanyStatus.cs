﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public enum ECompanyStatus
    {
        Active = 1,
        Inactive = 2
    }
}