﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    public enum EEndAction
    {

        ReleaseDocument = 0,
        CancelFlow = 1

    }
}