﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Enums.Workflow
{
    public enum EExecutionStatus
    {
        New = 0,
        Started = 1,
        FinishedRelease = 2,
        FinishedCancel = 3,
        Canceled = 4
    }
}