﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    public enum EStepAction
    {

        View = 0,
        Edit = 1,
        Download = 2,
        Opinion = 3

    }
}