﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    public enum ETriggerType
    {

        SendAndCheckin = 0,
        Checkin = 1,
        Send = 2

    }
}