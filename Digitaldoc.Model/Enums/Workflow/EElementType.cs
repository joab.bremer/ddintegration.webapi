﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.WF
{
    public enum EElementType
    {

        Review = 0,
        Notification = 1,
        Decision = 2,
        Approve = 3,
        ApproveWithSignature = 4,
        Training = 5,
        Distribution = 6,
        Analysis = 7,
        Start = 8,
        End = 9,
        ReleaseDocument = 10

    }
}