﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public enum EAdditionalFieldType
    {
        Boolean = 1,
        IntegerNumber = 2,
        Text = 3,
        DecimalNumber = 4,
        LongText = 5,
        Date = 6,
        List = 7
    }
}