﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public enum EAdditionalTreeItems
    {

        QuickAcces = -1,
        SharedDocuments = -2,
        PendingScans = -3,
        ActiveCheckouts = -4,
        Favorites = -5,
        TrashCan = -6,
        Search = -7

    }
}