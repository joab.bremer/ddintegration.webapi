﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public enum EMediaType
    {
        SimpleMedia = 1,
        SearchableMedia = 2,
    }
}