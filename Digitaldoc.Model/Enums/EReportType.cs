﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public enum EReportType
    {

        IndexedPages = 0,
        UsersActive = 1,
        UsersInactive = 2,
        WorkflowRun = 3,
        AddField = 4,
        Inventory = 5,
        ExpireDocument = 6
    }
}