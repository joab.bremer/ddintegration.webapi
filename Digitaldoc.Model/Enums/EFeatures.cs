﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public enum EFeatures
    {
        ChangeProperties = 1,
        ApproveDocumentInFlow = 2,
        CancelCheckout = 4,
        CancelFlow = 5,
        CheckoutCheckin = 6,
        SendDocument = 11,
        SendByEmail = 12,
        BecomeObsolete = 13,
        //Print = 14,
        NotifyUsers = 15,
        Download = 16,
        DownloadOldRevisions = 17,
        ShareDocument = 18,
        DeleteDocument = 20,
        RevertRevision = 21,
        ReviewDocumentInFlow = 22,
        ReplaceFile = 23,
        ViewDocument = 24,
        ViewOldRevision = 25,
        Print = 26,
        DigitalSinature = 27,
        LinkDocuments = 28,
    }
}