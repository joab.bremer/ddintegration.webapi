﻿namespace Digitaldoc.Model
{
    public enum ESortDirection
    {

        None = 0,
        Descending = 1,
        Ascending = 2

    }
}
