﻿namespace Digitaldoc.Model
{
    public enum ERevisionStatus
    {
        Approved = 1,
        NotApproved = 2,
    }
}