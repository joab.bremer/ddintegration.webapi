﻿namespace Digitaldoc.Model
{
    public enum EAdminFeatures
    {
        ConfigureDigitalization = 1,
        ConfigureSystemOptions = 2,
        ManageExtensions = 3,
        ManageGroups = 4,
        ManageUsers = 5,
        //ImportUsersOfDomain = 6,
        ManageRoles = 7,
        ManagePhysicalLocalizations = 8,
        //ManageAdminFeatures = 9,
        ManageRepositories = 10,
        ManageMediaTypes = 11,
        ManageDocumentTypes = 12,
        ManageWorkflows = 13,
        ManageDataLists = 14,
        ManageAdditionalFields = 15,
        ViewReports = 16,
        RegisterSystem = 17,
        ManageTrashCan = 18,
        ManageFolders = 19,
        ExportMedia = 20,
        ManageNFes = 21,
        AutoComplete = 22
    }
}