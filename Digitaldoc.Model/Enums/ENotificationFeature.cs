﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Enums
{
    /// <summary>
    /// Enum para gerenciar qual a "classificação" da notificação, se é de compartilhamento, aviso de documento, ou qualquer outra coisa que criaremos no sistema...
    /// </summary>
    public enum ENotificationFeature
    {
        Shared = 0,
        WorkflowTask = 1,
        WorkflowNotification = 2,
        GbsLimitEnding = 3,
        WelcomeNewUser = 4
    }
}