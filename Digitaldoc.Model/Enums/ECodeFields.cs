﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Enums
{
    public enum ECodeFields
    {

        Nothing = 0,
        FolderAcronym = 1,
        DocumentTypeAcronym = 2,
        Sequence = 3,
        RevisionNumber = 4,
        RevisionYear = 5,
        Extension = 6

    }
}