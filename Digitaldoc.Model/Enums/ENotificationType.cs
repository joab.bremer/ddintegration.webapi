﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Enums
{
    /// <summary>
    /// Qual o tipo de notificação, se será apenas por email, apenas sistema ou ambos.
    /// </summary>
    public enum ENotificationType
    {
        Email = 0,
        System = 1,
        Both = 2
    }
}