﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model
{
    public enum EAdditionalFieldValidationType
    {
        NO_VALIDATION = 0,
        CPF = 1,
        CNPJ = 2,
    }
}