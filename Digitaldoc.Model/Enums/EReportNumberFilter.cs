﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitaldoc.Model.Enums
{
    public enum EReportNumberFilter
    {

        EqualTo = 0,
        GreaterEqual = 1,
        LessEqual = 2

    }
}