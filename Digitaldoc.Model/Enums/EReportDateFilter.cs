﻿namespace Digitaldoc.Model
{
    public enum EReportDateFilter
    {

        Today = 0,
        LastWeek = 1,
        LastMonth = 2,
        BetweenDates = 3

    }
}