﻿namespace Digitaldoc.Model
{
    public enum EDocumentStatus
    {
        Enabled = 1,
        TrashCan = 2,
        InFlow = 3,
        Obsolete = 4,
        InCheckout = 5,
        WaitingFile = 6,
        InFlowInCheckout = 7,
    }
}