﻿using System;

namespace Digitaldoc.Model.Validation
{
    public class WebAPIValidationException : Exception
    {
        ValidationErrors _errors = null;

        public WebAPIValidationException(string message, ValidationErrors errors = null) : base(message)
        {
            _errors = errors;
        }

        public WebAPIValidationException(ValidationErrors errors)
            : base(errors != null ? errors.Message : "")
        {
            _errors = errors;
        }

        public ValidationErrors Errors
        {
            get { return _errors; }
            set { _errors = value; }
        }

    }
}