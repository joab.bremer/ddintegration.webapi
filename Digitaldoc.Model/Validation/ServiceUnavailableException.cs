﻿using System;

namespace Digitaldoc.Model.Validation
{
    public class ServiceUnavailableException : Exception
    {
        public ServiceUnavailableException() : base(Resources.Resources.ServiceUnavailableMessage)
        {
        }

        public ServiceUnavailableException(string message) : base(message)
        {

        }

        public ServiceUnavailableException(string message, Exception innerException) : base(message, innerException)
        {

        }

    }
}
