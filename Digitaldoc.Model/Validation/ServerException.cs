﻿using System;

namespace Digitaldoc.Model.Validation
{
    public class ServerException : Exception
    {
        public ServerException() : base(Resources.Resources.serverUnknownError)
        {

        }

        public ServerException(string message) : base(message)
        {

        }

        public ServerException(string message, Exception innerException) : base(message, innerException)
        {

        }

        public ServerException(Exception innerException) : base(Resources.Resources.serverUnknownError, innerException)
        {

        }
    }
}
