﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Digitaldoc.Model.Validation
{
    public class ValidationErrors : INotifyPropertyChanged
    {
        string _message;
        Dictionary<string, string[]> _modelState;

        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                NotifyOfPropertyChange();
            }
        }

        public Dictionary<string, string[]> ModelState
        {
            get { return _modelState; }
            set
            {
                _modelState = value;
                NotifyOfPropertyChange();
            }
        }

        public string[] this[string column]
        {
            get
            {
                if (_modelState == null) { return new string[0]; }

                var col = _modelState.Keys.FirstOrDefault(c => c == column || (c.Contains(".") && (c.Split('.')[1] == column || c.Split('.').Last() == column)));

                return col != null ? _modelState[col] : new string[0];
            }

            set
            {
                var col = _modelState.Keys.FirstOrDefault(c => c == column || (c.Contains(".") && (c.Split('.')[1] == column || c.Split('.').Last() == column)));

                if (col != null)
                {
                    _modelState[col] = value;
                }
            }
        }

        protected void NotifyOfPropertyChange([CallerMemberName]string property = null)
        {
            if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(property)); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
